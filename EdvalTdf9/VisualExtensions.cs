﻿using System;
using System.Runtime.InteropServices;
using System.Windows;
using System.Windows.Forms;
using System.Windows.Media;

namespace EdvalTdf9
{
    public static class VisualExtensions
    {
        public static IWin32Window GetIWin32Window(this Visual visual)
        {
            var source = PresentationSource.FromVisual(visual) as System.Windows.Interop.HwndSource;
            IWin32Window win = new OldWindow(source.Handle);
            return win;
        }

        private class OldWindow : IWin32Window
        {
            private readonly IntPtr _handle;

            public OldWindow(IntPtr handle)
            {
                _handle = handle;
            }

            IntPtr IWin32Window.Handle => _handle;
        }

        public sealed class KnownFolder
        {
            private KnownFolder(Guid guid)
            {
                Guid = guid;
            }

            public Guid Guid { get; }

            public static readonly KnownFolder Downloads = new KnownFolder(
                new Guid("374DE290-123F-4565-9164-39C4925E467B"));
        }

        [DllImport("shell32.dll", CharSet = CharSet.Unicode)]
        static extern int SHGetKnownFolderPath([MarshalAs(UnmanagedType.LPStruct)] Guid rfid, uint dwFlags,
            IntPtr hToken, out string pszPath);

        public static string GetKnownFolder(KnownFolder knownFolder)
        {
            string path;
            SHGetKnownFolderPath(knownFolder.Guid, 0, IntPtr.Zero, out path);
            return path;
        }
    }
}