﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using NUnit.Framework;
using tdf9.tests.Samples;
using Tdf9Extraction;
using Tdf9Extraction.Recognizer;

namespace tdf9.tests
{
    public sealed class render_tests
    {
        private readonly Tdf9Content _content;

        public render_tests()
        {
            var source = TypeRecognizer.Recognize(new Sample("Timetable 2017.Zip").FullPath);

            Assert.IsNotNull(source.Tdf9Source, "Expected TDF9 source");

            using (var s = source.Tdf9Source.Open())
            {
                _content = Tdf9Parser.ExtractFromXml(
                    s.MainSchedule,
                    "Timetable",
                    s.PreferencesStreams
                        .Select(elec => Tdf9Parser.ExtractFromXml(elec.Item1, elec.Item2, new Tdf9Content[0]))
                        .ToArray());
            }
        }

        [Test]
        [TestCase("BLAH", "BLAH")]
        [TestCase("BLAH.12", "BLAH.12")]
        [TestCase("BLAH-12", "BLAH-12")]
        [TestCase("JHS/JST", "\"JHS/JST\"")]
        [TestCase("JHS JST", "\"JHS JST\"")]
        public void clean_code(string input, string expected)
        {
            Assert.AreEqual(expected, NativeFormat.CleanCode(input));
        }

        [Test]
        public void WholeRender()
        {
            Dump(NativeFormat.Render(_content));
        }

        [Test]
        public void SHOULDNT_render_empty_tags()
        {
            foreach (var line in NativeFormat.Render(_content))
            {
                var match = Regex.Match(line, @"(\w\w=\s)");
                if (match.Success)
                {
                    Assert.Fail($"Found empty tag:\r\n{line}\r\n{new string(' ', match.Index)}^");
                }
            }
        }

        private static void Dump(IEnumerable<string> output)
        {
            foreach (var s in output)
            {
                Console.WriteLine(s);
            }
        }

        [Test]
        public void renders_10_days_for_2017()
        {
            var days = NativeFormat.RenderDays(_content).ToArray();
            Dump(days);
            Assert.AreEqual(10, days.Length);
        }

        [Test]
        public void renders_9_period_nums_for_2017_with_particular_item()
        {
            var periodNums = NativeFormat.RenderPeriodNums(_content).ToArray();
            Dump(periodNums);
            Assert.AreEqual(9, periodNums.Length);
        }
        
        [Test]
        public void each_period_num_has_not_null_mi_attribute()
        {
            var periodNums = NativeFormat.RenderPeriodNums(_content).ToArray();
            Dump(periodNums);

            foreach (var line in periodNums)
            {
                Assert.IsFalse(line.EndsWith("MI="), $"`{line}` has empty MI value");
                Assert.IsFalse(line.Contains("MI= "), $"`{line}` has empty MI value");
            }
        }

        [Test]
        public void rendres_90_periods()
        {
            var periods = NativeFormat.RenderPeriods(_content).ToArray();
            Dump(periods);
            Assert.AreEqual(90, periods.Length);
        }

        [Test]
        public void renders_133_teachers()
        {
            var teachers = NativeFormat.RenderTeachers(_content).ToArray();
            Dump(teachers);
            Assert.AreEqual(133, teachers.Length);
        }

        [Test]
        public void teacher_lines_SHOULDNT_contain_TEF_AND_PARTTIME_faculties()
        {
            var teachers =
                NativeFormat.RenderTeachers(_content)
                    .Select(EttEntry.Parse)
                    .ToArray();

            foreach (var entity in teachers)
            {
                var faculties = entity.Fields["FA"].Split(',').Select(x => x.ToLower()).ToArray();
                Assert.IsFalse(faculties.Contains("part time"), 
                    $"Faculties for `{entity.Code}` contains `{entity.Fields["FA"]}` but it shoudn't");
                Assert.IsFalse(faculties.Contains("tef"), 
                    $"Faculties for `{entity.Code}` contains `{entity.Fields["FA"]}` but it shoudn't");
            }
            
            Assert.IsFalse(
                teachers.All(t => string.IsNullOrWhiteSpace(t.Fields["FA"])),
                "Teacher with faculties wasn't found");
        }

        [Test]
        public void SHOULD_NOT_export_junk_faculties()
        {
            var faculties =
                new HashSet<string>(
                    NativeFormat.RenderFaculties(_content)
                        .Select(x => x.Replace("CO=0xffffff", "").Trim().ToLower())
                        .ToArray());

            Dump(faculties);

            var junk = new HashSet<string>(new[] { "Parttime", "all", "PTDAY", "10sci"}.Select(x => x.ToLower()));

            var intersection = junk.Intersect(faculties);
            
            Assert.IsEmpty(intersection);
        }

        [Test]
        public void renders_77_rooms()
        {
            var sets = NativeFormat.RenderRooms(_content).ToArray();
            Dump(sets);
            Assert.AreEqual(77, sets.Length);
            Assert.Contains(
                "A204 CA=28 NB=\"A204 LIB LIBRARY\" GU=\"{514D475F-4C73-4AB7-82A3-FECBC01227E9}\"",
                sets);
        }

        [Test]
        public void render_parameters()
        {
            var parameters = NativeFormat.RenderParameters(_content).ToArray();
            Dump(parameters);
            Assert.AreEqual(" YY=2017 YR=12 FA=ENG ET=11_elecs", parameters[0]);
        }

        [Test]
        public void renders_773_classes()
        {
            var classes = NativeFormat.RenderClasses(_content).ToArray();
            Dump(classes);
            Assert.AreEqual(1562, classes.Length, "Output rows count should be 2 * non-empty classes count");
            Assert.IsTrue(classes.Any(x => x.StartsWith(@"12G.AE")));
            Assert.IsTrue(classes.Any(x => x.Contains("[TE=THM TI=D4-3 RO=J105]")));

            var years = classes
                .Select(x => Regex.Match(x, @"\s+YR\=(?<year>[\S]*)"))
                .Where(x => x.Success)
                .Select(yr => yr.Groups["year"].Value)
                .ToArray();
            
            Assert.IsNotEmpty(years);

            foreach (var yr in years)
            {
                Assert.IsTrue(
                    yr.IsNonNegativeInteger(),
                    $"Expected non negative integer but found {yr}");
            }
        }

        [Test]
        public void each_class_has_a_period()
        {
            var assignments = NativeFormat.RenderClasses(_content)
                    .Where(x => x.StartsWith("\t"))
                    .ToArray();
            
            Assert.IsNotEmpty(assignments, "Assignment haven't been found");

            foreach (var assignment in assignments)
            {
                var periods = Regex.Matches(assignment, @"\[(?<values>.*?)\]")
                    .Cast<Match>()
                    .Select(period =>  period.Groups["values"].Value)
                    .ToArray();
                
                Assert.IsNotEmpty(periods, $"Class with no periods: {assignment}");

                foreach (var period in periods)
                {
                    var values = period.Split(' ').Select(x => x.Split('='))
                        .ToDictionary(x => x[0], x => x[1]);
                    
                    Assert.IsTrue(values.ContainsKey("TI"), $"[{period}] doesn't have TI value (expected period)");
                }  
            }
        }

        [Test]
        public void renders_114_allowances()
        {
            var allowances = NativeFormat.RenderAllowances(_content).ToArray();
            Dump(allowances);
            Assert.AreEqual(114, allowances.Length);
            Assert.Contains(@"EngMent1 LN=""English Mentoring1"" LD=2 TE=CLM", allowances);
        }

        [Test]
        public void any_line_that_has_equal_sign_has_code_either_enquoted_or_valid()
        {
            var lines = NativeFormat.Render(_content);
            
            var linesWithData = lines.Where(x => Regex.IsMatch(x, @"^\S")).ToArray();
            
            Assert.IsNotEmpty(linesWithData);

            var codeLines = linesWithData.Where(x => x.Contains('=')).ToArray();
            
            Assert.IsNotEmpty(codeLines);

            var parsed = codeLines
                .Select(x => new
                {
                    Line = x,
                    Match = Regex.Match(x, @"(?<code>\S+)=")
                })
                .Where(x => x.Match.Success && x.Match.Groups["code"].Success)
                .ToArray();
            
            foreach (var line in parsed)
            {
                var code = line.Match.Groups["code"].Value.Trim();
                
                Assert.IsTrue(
                    Regex.IsMatch(code, @"^[a-zA-Z0-9\-\.]+$") || Regex.IsMatch(code, @"^\""[^\""]+\""$"),
                    $"[{code}] in line `{line.Line}` is not valid identifier or quoted code");
            }
            
            Console.WriteLine($"Checked {parsed.Length} lines");
        }

        [Test]
        public void shouldnt_contain_times_with_leading_zeroes()
        {
            var lines = NativeFormat.Render(_content);

            foreach (var line in lines)
            {
                Assert.IsFalse(Regex.IsMatch(line, @"0\d\:\d\d"), $"Seems like {line} contains time with leading zero");
            }
        }

        [Test]
        [TestCase("08:00", "8:00")]
        [TestCase("9:00", "9:00")]
        [TestCase("11:20", "11:20")]
        [TestCase("18:20", "18:20")]
        public void trim_time(string time, string expected)
        {
            Assert.AreEqual(expected, time.TrimTime());
        }

        [Test]
        [TestCase("D1-1", "D1-1")]
        [TestCase("D3-P", "D3P")]
        [TestCase("D10-P", "D10P")]
        public void normalize_period_code(string input, string expected)
        {
            Assert.AreEqual(expected, NativeFormat.NormalizePeriodCode(input));
        }

        [Test]
        public void there_are_7_files_with_electives()
        {
            Assert.AreEqual(7, _content.Electives.Length);
        }

        [Test]
        public void render_elective_things()
        {
            var electiveThings = NativeFormat.RenderElectiveThings(_content).ToArray();
            
            Dump(electiveThings);
        }

        [Test]
        public void render_elective_things_courses()
        {
            var lines = NativeFormat
                .RenderElectiveThing(
                    _content.Electives.First(x => x.FileName.StartsWith("Y11")),
                    _content.Students.GroupById("StudentGUID"))
                .ToArray();
            Dump(lines);
        }
    }
}