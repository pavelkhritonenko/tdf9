﻿using System;
using System.IO;
using System.Reflection;

namespace tdf9.tests.Samples
{
    public sealed class Sample
    {
        private static string AssemblyDirectory
        {
            get
            {
                var assembly = Assembly.GetExecutingAssembly();
                var codeBase = assembly.CodeBase;
                var uri = new UriBuilder(codeBase);
                var path = Uri.UnescapeDataString(uri.Path);
                return Path.GetDirectoryName(path);
            }
        }

        private static readonly string SamplesFolder = Path.Combine(AssemblyDirectory, "samples");
        
        private readonly string _relativePath;

        public Sample(string relativePath)
        {
            _relativePath = relativePath;
        }

        public string FullPath => Path.Combine(SamplesFolder, _relativePath);
    }
}