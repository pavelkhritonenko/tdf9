﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Xml;
using System.Xml.Linq;
using Tdf9Extraction.Recognizer;
using Tdf9Extraction.Transform;

namespace Tdf9Extraction
{
    public static class Tdf9Parser
    {
        private static Tdf9Content ReadContentFromXml(Stream stream, string fileName, Tdf9Content[] electives)
        {
            var content = new Dictionary<string, Dictionary<string, string>[]>();

            using (var reader = XmlReader.Create(stream))
            {
                var doc = XDocument.Load(reader);

                if (doc.Root == null) throw new InvalidOperationException("Root not found in xml file");

                foreach (var collection in doc.Root.Elements().Where(x => x.Name.LocalName != "Settings"))
                {
                    var entries = ParseCollection(collection).ToArray();
                    var collectionName = collection.Name.LocalName;

                    if (entries.Any())
                    {
                        content.Add(collectionName, entries);
                    }
                }
            }
            
            return new Tdf9Content(content, fileName, electives);
        }

        private static Dictionary<TK, TI> ToDictionary<TK, TI>(IEnumerable<TI> seq, Func<TI, TK> keyFunc)
        {
            return seq
                .ToLookup(keyFunc)
                .ToDictionary(x => x.Key, x => x.First());
        }

        private static Dictionary<TK, TR> ToDictionary<TK, TI, TR>(
            IEnumerable<TI> seq,
            Func<TI, TK> keyFunc,
            Func<TI, TR> resultFunc)
        {
            return seq
                .ToLookup(keyFunc)
                .ToDictionary(x => x.Key, x => resultFunc(x.First()));
        }

        private static Dictionary<string, Dictionary<string, string>[]> Join(
            Dictionary<string, Dictionary<string, string>[]> first,
            Dictionary<string, Dictionary<string, string>[]> second)
        {
            return first
                .Concat(second)
                .GroupBy(x => x.Key)
                .ToDictionary(x => x.Key, x => x.SelectMany(t => t.Value ?? new Dictionary<string, string>[0])
                                                .ToArray());
        }

        public static IEnumerable<Tdf9Content> GetElectives(Tdf9Container container)
        {
            return container.PreferencesStreams
                .Select(x => ExtractFromXml(x.Item1, x.Item2, new Tdf9Content[0]));
        }

        public static string ExtractTdf9FromStream(
            Tdf9Container container,
            string sourceFileName,
            string outDir = null)
        {
            var dirs = Utils.CreateOutputDir(sourceFileName, outDir);
            
            var electives = GetElectives(container).ToArray();

            var mainContent = ExtractFromXml(container.MainSchedule, sourceFileName, electives);
            WriteContent(dirs, mainContent);
            NativeFormat.Export(dirs, mainContent);

            var combined = new Dictionary<string, Dictionary<string, string>[]>();
            foreach (var electiveThing in electives)
            {
                var optDirs = dirs.CreatePreferencesFolder(electiveThing.FileName);
                WriteContent(optDirs, electiveThing);
                combined = Join(combined, electiveThing.Content);
            }

            WriteContent(
                dirs.CreatePreferencesFolder("all-preferences"),
                new Tdf9Content(combined, sourceFileName, new Tdf9Content[0]));

            return dirs.MainDir;
        }

        public static Tdf9Content ExtractFromXml(Stream stream, string fileName, Tdf9Content[] electives)
        {
            var content = ReadContentFromXml(stream, fileName, electives);

            NormalizeRoomCodes(content);
            NormalizeFaculties(content);
            NormalizeAllowances(content);

            Classes.RenameWithDotNotation(content.Classes);

            EnrichPublishedTimetables(content.PublishedTimetables);

            EnrichRurRooms(content);

            EnrichTeachers(content);

            EnrichMeetingTeachers(content);
            EnrichFacultyTeachers(content);

            EnrichTeachersByFaculty(content);

            EnrichUnscheduledDuties(content);

            EnrichRollClasses(content);

            EnrichMrcg(content);

            EnrichPeriods(content);

            EnrichYardDuties(content);

            EnrichTeacherUnavailables(content);
            EnrichRoomUnavailables(content);

            EnrichTimetable(content);

            EnrichClasses(content);
            RemoveClassDuplicates(content);
            EnrichEdvalClassCodes(content);
            EnrichTimetableWithEdvalClassCode(content);

            EnrichCourses(content);
            EnrichStudents(content);

            EnrichClassesByMrcg(content);

            EnrichPreferences(content);

            EnrichOptions(content);

            EnrichConstraintOptions(content);

            return content;
        }

        private static void NormalizeAllowances(Tdf9Content content)
        {
            content.GetOrNull("UnscheduledDuties")?
                .Iter(d => d["Code"] = d["Code"].NormalizeCode());
        }

        private static void NormalizeFaculties(Tdf9Content content)
        {
            content.GetOrCreate("Faculties")
                .Iter(x => x["Code"] = x["Code"].NormalizeCode());
        }

        private static IEnumerable<Dictionary<string, string>> DeduplicateClasses(Tdf9Content content)
        {
            var duplicates = content.Timetables.Where(x => !string.IsNullOrWhiteSpace(x["ClassID"]))
                .GroupBy(x => x["ClassID"])
                .Select(x => new
                {
                    ClassID = x.Key,
                    Years = x.Select(t => t["Year"]).Distinct().ToArray()
                })
                .Where(x => x.Years.Count() > 1)
                .ToDictionary(x => x.ClassID, x => x.Years);

            foreach (var cl in content.Classes)
            {
                if (!duplicates.ContainsKey(cl["ClassGUID"]))
                {
                    yield return cl;
                }
                else
                {
                    var years = duplicates[cl["ClassGUID"]];

                    foreach (var yr in years)
                    {
                        yield return new Dictionary<string, string>(cl) {["Year"] = yr};
                    }
                }
            }
        }

        private static void RemoveClassDuplicates(Tdf9Content content)
        {
            content.Classes = DeduplicateClasses(content).ToArray();
        }

        private static void NormalizeRoomCodes(Tdf9Content content)
        {
            foreach (var room in content.Rooms)
            {
                room["Code"] = room["Code"].NormalizeCode();
            }
        }

        private static void WriteContent(Utils.OutDirs dirs, Tdf9Content content)
        {
            foreach (var collectionName in content.Keys)
            {
                WriteCollection(collectionName, content.GetOrNull(collectionName), dirs);
            }
        }

        private static void EnrichConstraintOptions(Tdf9Content content)
        {
            var constraintOptions = content.GetOrNull("ConstraintOptions");
            if (constraintOptions == null) return;
            var constraints = content.GetOrCreate("Constraints")
                .GroupById("ConstraintID");
            var options = content.GetOrCreate("Options")
                .GroupById("OptionID");

            constraintOptions.Iter(row =>
            {
                LookReference(row, options, "OptionID", "SubjectCode", "SubjectCode");
                LookReference(row, options, "OptionID", "SubjectName", "SubjectName");
                LookReference(row, options, "OptionID", "AlternateName", "AlternateName");
                LookReference(row, options, "OptionID", "AlternateCode", "AlternateCode");

                LookReference(row, constraints, "ConstraintID", "GroupNo", "GroupNo");
                LookReference(row, constraints, "ConstraintID", "TypeOf", "TypeOf");
                LookReference(row, constraints, "ConstraintID", "Limit", "Limit");
                LookReference(row, constraints, "ConstraintID", "Note", "Note");
            });
        }

        private static void EnrichOptions(Tdf9Content content)
        {
            var options = content.GetOrNull("Options");
            if (options == null) return;


            var subjects = content.GetOrCreate("Subjects")
                .GroupById("SubjectID");

            options.Iter(row =>
            {
                LookReference(row, subjects, "SubjectID", "SubjectCode", "SubjectCode");
                LookReference(row, subjects, "SubjectID", "SubjectName", "SubjectName");
            });
        }

        private static void EnrichPreferences(Tdf9Content content)
        {
            var preferences = content.GetOrNull("Preferences");

            if (preferences == null) return;

            var students = content.GetOrCreate("Students")
                .GroupById("StudentGUID");

            var classes = content.GetOrCreate("Classes")
                .GroupById("ClassGUID");

            var options = content.GetOrCreate("Options")
                .GroupById("OptionID");


            preferences.Iter(row =>
            {
                row["Student Full Name"] = students.GetOrDefault(Guid.Parse(row["StudentID"]))["FullName"];
                row["Year"] = students.GetOrDefault(Guid.Parse(row["StudentID"]))["YearLevel"];
                Guid classId;
                if (row.ContainsKey("ClassID") && Guid.TryParse(row["ClassID"], out classId))
                    row["Class"] = classes.GetOrDefault(classId)["ClassCode"];

                Guid optionId;
                if (row.ContainsKey("OptionID") && Guid.TryParse(row["OptionID"], out optionId))
                {
                    row["OptionName"] = options.GetOrDefault(optionId)["AlternateName"];
                    row["OptionCode"] = options.GetOrDefault(optionId)["AlternateCode"];
                }
                RenameField(row, "PreferenceNo", "Preference #");
            });
        }


        private static void EnrichClassesByMrcg(Tdf9Content content)
        {
            var courses = content.GetOrCreate("Courses")
                .Select(x => TryPickMapByGuid(x, "ClassID", g => new
                {
                    ClassID = g,
                    MrcgId = x.GetOrDefault("MRCGID", ""),
                    Spread = x.GetOrDefault("Spread", "")
                }))
                .Where(x => x != null)
                .GroupBy(x => x.ClassID)
                .ToDictionary(x => x.Key, x => x.FirstOrDefault());

            content.GetOrNull("Classes")?.Iter(cl =>
            {
                cl["MRCGID"] = TryPickMapByGuid(cl, "ClassGUID", g => courses.GetOrDefault(g)?.MrcgId ?? "");
                cl["Spread"] = TryPickMapByGuid(cl, "ClassGUID", g => courses.GetOrDefault(g)?.Spread ?? "");
                cl["Year"] = cl.GetOrDefault("Year", "Meetings");
                if (cl["Year"] == "Meetings")
                {
                    cl["Spread"] = "Anything";
                }
            });


        }

        private static void EnrichTeachersByFaculty(Tdf9Content content)
        {
            var faculties = content.GetOrCreate("FacultyTeachers")
                .GroupBy(x => Guid.Parse(x["TeacherID"]))
                .ToDictionary(x => x.Key, x => x.Select(f => f.GetOrDefault("Faculty", "")));

            content.GetOrNull("Teachers")?.Iter(entry =>
            {
                var teacherFaculties = faculties.GetOrDefault(Guid.Parse(entry["TeacherID"]), new string[0]);
                entry["Faculty"] = string.Join(",", NonEmptyUnique(teacherFaculties));
            });
        }

        private static void EnrichTimetableWithEdvalClassCode(Tdf9Content content)
        {
            var classes =
                ToDictionary(
                    content.GetOrCreate("Classes"),
                    x => Guid.Parse(x["ClassGUID"]),
                    x => x["EdvalClassCode"]);

            content.GetOrNull("Timetables")?.Iter(entry =>
            {
                entry["EdvalClassCode"] = TryPickMapByGuid(entry, "ClassID", g => classes.GetOrDefault(g, ""));
            });
        }

        private static void EnrichCourses(Tdf9Content content)
        {
            var classes = ToDictionary(
                content.GetOrCreate("Classes"),
                x => Guid.Parse(x["ClassGUID"]),
                x => x.GetOrDefault("Year", ""));

            var classGroups = content
                .GetOrCreate("ClassGroups")
                .ToDictionary(x => Guid.Parse(x["ClassGroupID"]), x => new
                {
                    MrcgId = x["MRCGID"],
                    Spread = x["Spread"]
                });

            content.GetOrNull("Courses")?.Iter(course =>
            {
                course["Year"] = TryPickMapByGuid(course, "ClassID", g => classes.GetOrDefault(g, ""));
                course["Spread"] = TryPickMapByGuid(course, "ClassGroupID", g => classGroups.GetOrDefault(g)?.Spread ?? "");
                course["MRCGID"] = TryPickMapByGuid(course, "ClassGroupID", g => classGroups.GetOrDefault(g)?.MrcgId ?? "");
            });
        }

        private static void EnrichMrcg(Tdf9Content content)
        {
            var rollClasses = content
                .GetOrCreate("RollClasses")
                .ToDictionary(x => Guid.Parse(x["RollClassID"]));

            var mrcg = content
                .GetOrCreate("MRCGClassGroups")
                .ToDictionary(x => Guid.Parse(x["ClassGroupID"]), x => x["MRCGID"]);

            content.GetOrNull("ClassGroups")?.Iter(entry =>
            {
                entry["RollClass"] = TryPickMapByGuid(entry, "RollClassID", g => rollClasses.GetOrDefault(g, new Dictionary<string, string>()).GetOrDefault("Code"));
                entry["MRCGID"] = TryPickMapByGuid(entry, "ClassGroupID", g => mrcg.GetOrDefault(g, ""));
                entry["Spread"] = GetSpreadFromClassGroup(entry);
            });

            if (content.ClassGroups != null)
            {
                var classGroups = content
                    .ClassGroups
                    .ToDictionary(x => Guid.Parse(x["ClassGroupID"]), x => x.GetOrDefault("RollClass"));

                var mrcgs = content
                    .GetOrCreate("MRCGs")
                    .ToDictionary(x => Guid.Parse(x["MRCGID"]), x => x.GetOrDefault("Code", ""));

                content.GetOrNull("MRCGClassGroups")?.Iter(entry =>
                {
                    entry["MRCG"] = TryPickMapByGuid(entry, "MRCGID", g => mrcgs.GetOrDefault(g));
                    entry["RollClass"] = TryPickMapByGuid(entry, "ClassGroupID", g => classGroups.GetOrDefault(g));
                });
            }
        }

        private static string GetSpreadFromClassGroup(Dictionary<string, string> entry)
        {
            var str = $"{entry["Doubles"]},{entry["Triples"]},{entry["Quadruples"]}";
            switch (str)
            {
                case "0,0,0":
                    return "NoDoubles";
                case "1,0,0":
                    return "Double";
                case "2,0,0":
                    return "TwoDoubles";
                case "3,0,0":
                    return "ThreeDoubles";
                case "4,0,0":
                    return "AllDoubles";
                case "0,1,0":
                    return "Triple";
                case "0,2,0":
                    return "Triple";
                case "0,0,1":
                    return "Quadruple";
            }

            Func<int, string, string> repeat =
                (count, ch) => string.Join(",", Enumerable.Range(0, count).Select(_ => ch));

            var doubles = repeat(int.Parse(entry["Doubles"]), "2");
            var triples = repeat(int.Parse(entry["Doubles"]), "3");
            var quadruples = repeat(int.Parse(entry["Doubles"]), "4");

            var expr = string.Join(",", doubles, triples, quadruples);
            return $"({expr})";
        }

        private static void EnrichYardDuties(Tdf9Content content)
        {
            var periods = content
                .GetOrCreate("Periods")
                .ToDictionary(x => Guid.Parse(x["PeriodID"]), x => x.GetOrDefault("Day") + x.GetOrDefault("PeriodCode"));

            content.GetOrNull("YardDutySessions")?.Iter(entry =>
            {
                entry["Period"] = TryPickMapByGuid(entry, "PeriodID", g => periods.GetOrDefault(g, ""));
            });

            var sessions = content
                .GetOrCreate("YardDutySessions")
                .ToDictionary(x => Guid.Parse(x["YardDutySessionID"]));

            var areas = content
                .GetOrCreate("YardDutyAreas")
                .ToDictionary(x => Guid.Parse(x["YardDutyAreaID"]), x => x.GetOrDefault("Code"));

            var teachers = content
                .GetOrCreate("Teachers")
                .ToDictionary(x => Guid.Parse(x["TeacherID"]), x => x.GetOrDefault("Code"));

            content.GetOrNull("YardDuties")?.Iter(entry =>
            {
                entry["YardDutyArea"] = TryPickMapByGuid(entry, "YardDutyAreaID", g => areas.GetOrDefault(g, ""));
                entry["YardDutySession"] = TryPickMapByGuid(entry, "YardDutySessionID", g => sessions.GetOrDefault(g, new Dictionary<string, string>()).GetOrDefault("Code"));
                entry["Period"] = TryPickMapByGuid(entry, "YardDutySessionID", g => sessions.GetOrDefault(g, new Dictionary<string, string>()).GetOrDefault("Period"));
                entry["Teacher"] = TryPickMapByGuid(entry, "TeacherID", g => teachers.GetOrDefault(g, ""));
            });

            content.GetOrNull("YardDutyAreas")?.Iter(entry =>
            {
                RenameField(entry, "Site", "Campus");
            });
        }

        public static T TryPickMapByGuid<T>(Dictionary<string, string> entry, string field, Func<Guid, T> f)
        {
            Guid id;
            return Guid.TryParse(entry.GetOrDefault(field), out id) ? f(id) : default(T);
        }

        public static string TryPickMapByGuid(Dictionary<string, string> entry, string field, Func<Guid, string> f)
        {
            return TryPickMapByGuid<string>(entry, field, f) ?? "";
        }

        private static void EnrichMeetingTeachers(Tdf9Content content)
        {
            var meetings = content
                .GetOrCreate("Meetings")
                .ToDictionary(x => Guid.Parse(x["MeetingID"]));

            var teachers = content
                .GetOrCreate("Teachers")
                .ToDictionary(x => Guid.Parse(x["TeacherID"]));

            content.GetOrNull("MeetingTeachers")?.Iter(entry =>
            {
                LookReference(entry, teachers, "TeacherID", "Teacher", "Code");
                LookReference(entry, meetings, "MeetingID", "Meeting", "Code");
            });

            var byMeeting = content
                .GetOrCreate("MeetingTeachers")
                .GroupBy(x => Guid.Parse(x["MeetingID"]))
                .ToDictionary(x => x.Key, x => NonEmptyUnique(x.Select(t => t.GetOrDefault("Teacher", ""))));

            meetings.Iter(p =>
            {
                p.Value["TeacherPref"] = string.Join(",", byMeeting.GetOrDefault(p.Key, new string[0]));
                p.Value["ClassCode"] = $"Meet{p.Value.GetOrDefault("Code", "")}";
                p.Value["Year"] = "Meetings";

                double load;
                if (double.TryParse(p.Value.GetOrDefault("Load"), out load))
                {
                    var loadStr = (load / 100).ToString("0.##", CultureInfo.InvariantCulture);
                    p.Value["#Per"] = $"1({loadStr})";
                }

                RenameField(p.Value, "Code", "MeetingCode");
            });
        }

        public static void DivideBy100(Dictionary<string, string> entry, string field)
        {
            double load;
            if (double.TryParse(entry.GetOrDefault(field), out load))
            {
                entry[field] = (load / 100).ToString("0.##", CultureInfo.InvariantCulture);
            }
        }


        private static void EnrichFacultyTeachers(Tdf9Content content)
        {
            var faculties = content
                .GetOrCreate("Faculties")
                .ToDictionary(x => Guid.Parse(x["FacultyID"]));

            var teachers = content
                .GetOrCreate("Teachers")
                .ToDictionary(x => Guid.Parse(x["TeacherID"]));

            content.GetOrNull("FacultyTeachers")?.Iter(entry =>
            {
                LookReference(entry, teachers, "TeacherID", "Teacher", "Code");
                LookReference(entry, faculties, "FacultyID", "Faculty", "Code");
            });
        }

        private static void EnrichPublishedTimetables(IEnumerable<Dictionary<string, string>> content)
        {
            content.Iter(x =>
            {
                x["PublishDate"] = ParseDate(x.GetOrDefault("PublishDate", ""));
                x["StartDate"] = ParseDate(x.GetOrDefault("StartDate", ""));
            });
        }

        private static string ParseDate(string date)
        {
            if (string.IsNullOrWhiteSpace(date)) return "";
            double days;
            return !double.TryParse(date, NumberStyles.AllowDecimalPoint, CultureInfo.InvariantCulture, out days)
                   ? ""
                   : new DateTime(1900, 1, 1).AddDays(days).ToString("G", CultureInfo.CurrentCulture);
        }

        private static void EnrichRurRooms(Tdf9Content content)
        {
            var rooms = content.Rooms
                .ToDictionary(x => Guid.Parse(x["RoomID"]), x => x.GetOrDefault("Code"));
            var rurs = content
                .GetOrCreate("RURs")
                .ToDictionary(x => Guid.Parse(x["RURID"]), x => x.GetOrDefault("Code"));
            content.GetOrNull("RURRooms")?.Iter(entry =>
            {
                Guid rurId;
                if (Guid.TryParse(entry["RURID"], out rurId))
                {
                    entry["RUR"] = rurs.GetOrDefault(rurId, "");
                }
                else
                {
                    entry["RUR"] = "";
                }

                Guid roomId;
                if (Guid.TryParse(entry["RoomID"], out roomId))
                {
                    entry["Room"] = rooms.GetOrDefault(roomId, "");
                }
                else
                {
                    entry["Room"] = "";
                }

            });
        }

        private static void EnrichUnscheduledDuties(Tdf9Content content)
        {
            var teachers = content
                .GetOrCreate("Teachers")
                .ToDictionary(x => Guid.Parse(x["TeacherID"]), x => x.GetOrDefault("Code"));

            content.GetOrNull("UnscheduledDuties")?.Iter(entry =>
            {
                Guid teacherId;
                if (Guid.TryParse(entry.GetOrDefault("TeacherID", ""), out teacherId))
                {
                    entry["Teacher"] = teachers.GetOrDefault(teacherId, "");
                }

                double load;
                if (double.TryParse(entry["Load"], out load))
                {
                    entry["Load"] = (load / 100).ToString("0.##", CultureInfo.InvariantCulture);
                }

                RenameField(entry, "Name", "Allowance");

            });

            content.RenameCollection("UnscheduledDuties", "Allowances");
        }

        private static void EnrichRoomUnavailables(Tdf9Content content)
        {
            var roomUnavailablesDict = content.GetOrCreate("RoomUnavailables");
            var periods = content
                .GetOrCreate("Periods")
                .ToDictionary(x => Guid.Parse(x["PeriodID"]), x => x.GetOrDefault("Day") + x.GetOrDefault("PeriodCode"));

            var rooms = content
                .GetOrCreate("Rooms")
                .ToDictionary(x => Guid.Parse(x["RoomID"]), x => x.GetOrDefault("Code", ""));

            roomUnavailablesDict.Iter(room =>
            {
                room["Room"] = TryPickMapByGuid(room, "RoomID", g => rooms.GetOrDefault(g, ""));
                room["PeriodGUID"] = room.GetOrDefault("PeriodID");
                room["PeriodID"] = periods.GetOrDefault(Guid.Parse(room.GetOrDefault("PeriodID")));
            });

            var roomUnavailables = roomUnavailablesDict
                .GroupBy(x => Guid.Parse(x["RoomID"]))
                .ToDictionary(x => x.Key, x => x.Select(p => p.GetOrDefault("PeriodID")));

            if (content.Rooms != null)
            {
                content.GetOrNull("Rooms").Iter(room =>
                {
                    room["Size"] = room.GetOrDefault("Seats");
                    room.Remove("Seats");

                    var id = Guid.Parse(room["RoomID"]);
                    if (!roomUnavailables.ContainsKey(id))
                    {
                        room["Unavailable"] = "";
                    }
                    else
                    {
                        var unavailable = NonEmptyUnique(roomUnavailables[id]);
                        room["Unavailable"] = string.Join(",", unavailable);
                    }

                    room["Campus"] = room.GetOrDefault("SiteNo");
                    room.Remove("SiteNo");

                    if (room.GetOrDefault("Name", "") != room.GetOrDefault("Code", ""))
                    {
                        room["Notes"] = string.Join(" ", new[]
                        {
                            room.GetOrDefault("Name", ""),
                            room.GetOrDefault("Notes", "")
                        }.Where(x => !string.IsNullOrWhiteSpace(x)));
                    }
                });
            }
        }

        private static void EnrichTeacherUnavailables(Tdf9Content content)
        {
            var periods = content.GetOrNull("Periods")?.ToDictionary(x => Guid.Parse(x["PeriodID"]));
            var teachers = content
                .GetOrCreate("Teachers")
                .ToDictionary(x => Guid.Parse(x["TeacherID"]), x => x.GetOrDefault("Code"));

            var teacherUnavailabilitiesDict = content.GetOrNull("TeacherUnavailables");

            if (teacherUnavailabilitiesDict == null) return;

            teacherUnavailabilitiesDict.Iter(entry =>
            {
                Guid periodId;
                if (!Guid.TryParse(entry.GetOrDefault("PeriodID", ""), out periodId)) return;
                var period = periods.GetOrDefault(periodId);
                if (period == null) return;
                entry["PeriodGUID"] = entry.GetOrDefault("PeriodID", "");

                var divider = period["PeriodCode"].IsNonNegativeInteger() ? "-" : "";
                
                entry["PeriodID"] = $"{period["Day"]}{divider}{period["PeriodCode"]}";
                entry["Teacher"] = TryPickMapByGuid(entry, "TeacherID", g => teachers.GetOrDefault(g, ""));
            });

            var teacherUnavailabilities = teacherUnavailabilitiesDict
                .Select(x => new
                {
                    TeacherId = Guid.Parse(x["TeacherID"]),
                    PeriodId = x["PeriodID"],
                    Unavailable = bool.Parse(x["Unavailable"])
                })
                .Select(x => new
                {
                    x.TeacherId,
                    PeriodId = x.PeriodId
                })
                .GroupBy(x => x.TeacherId)
                .ToDictionary(x => x.Key, x => x.Select(t => t.PeriodId));

            content.GetOrNull("Teachers")?.Iter(teacher =>
            {
                var id = Guid.Parse(teacher["TeacherID"]);
                if (!teacherUnavailabilities.ContainsKey(id)) return;
                var unavailable = NonEmptyUnique(teacher["Unavailable"].Yield().Concat(teacherUnavailabilities[id]));
                teacher["Unavailable"] = string.Join(",", unavailable);
            });
        }

        private static void EnrichStudents(Tdf9Content content)
        {
            var studentsDict = content.GetOrCreate("Students");

            content.GetOrNull("StudentLessons")?.Iter(x =>
            {
                x["ClassCode"] = Regex.Replace(x.GetOrDefault("ClassCode", ""), @"\s+", ".");
            });

            foreach (var entry in studentsDict)
            {
                var lastName = (entry.GetOrDefault("FamilyName") ?? entry.GetOrDefault("LastName") ?? "")
                    .ToUpper();

                var firstName = entry.GetOrDefault("FirstName", "").ToTitleCase();
                var preferredName = entry.GetOrDefault("PreferredName", "").ToTitleCase();
                var shortName = string.IsNullOrWhiteSpace(preferredName) ? firstName : preferredName;
                entry["FullName"] = $"{lastName}, {shortName}".Trim();

            }

            var students = ToDictionary(studentsDict, x => Guid.Parse(x["StudentID"]));

            var classes = content.GetOrCreate("Classes")
                .GroupBy(x => x["Code"])
                .ToDictionary(x => x.Key, x => new
                {
                    Year = x.First().GetOrDefault("Year", ""),
                    EdvalClassCode = x.First().GetOrDefault("EdvalClassCode", "")
                });

            content.GetOrNull("StudentLessons")?.Iter(x =>
            {
                var student = students.GetOrDefault(Guid.Parse(x["StudentID"]), new Dictionary<string, string>());
                x["StudentName"] = student.GetOrDefault("FullName");
                x["StudentCode"] = student.GetOrDefault("Code");
                x["Year"] = classes.GetOrDefault(x.GetOrDefault("ClassCode", ""))?.Year ?? "";
                x["EdvalClassCode"] = classes.GetOrDefault(x.GetOrDefault("ClassCode", ""))?.EdvalClassCode ?? "";

                RenameField(x, "StudentID", "StudentGUID");
                RenameField(x, "CourseID", "LessonGUID");
            });

            EnrichStudentsByClasses(content);

            content.GetOrNull("Students")?.Iter(x =>
            {
                RenameField(x, "Code", "StudentCode");
                RenameField(x, "StudentID", "StudentGUID");
                RenameField(x, "FirstName", ".FirstName");
                RenameField(x, "FamilyName", ".Surname");
                RenameField(x, "PreferredName", ".PreferredName");
                RenameField(x, "RollClass", "GroupLetter");
            });
        }

        private static void EnrichStudentsByClasses(Tdf9Content content)
        {
            var studentsDict = content.GetOrCreate("Students");
            var maxClasses = 0;
            var studentLessons = content
                .GetOrCreate("StudentLessons")
                .GroupBy(x => Guid.Parse(x["StudentGUID"]))
                .ToDictionary(x => x.Key, x => new
                {
                    Classes = NonEmptyUnique(x.Select(c => c.GetOrDefault("ClassCode", ""))),
                    EdvalClasses = NonEmptyUnique(x.Select(c => c.GetOrDefault("EdvalClassCode", "")))
                });

            studentsDict.Iter(entry =>
            {
                studentLessons.GetOrDefault(Guid.Parse(entry["StudentID"]))?.Classes.Iteri((s, ind) =>
                {
                    maxClasses = Math.Max(maxClasses, ind);
                    entry[$"Class{ind + 1}"] = s;
                });

                studentLessons.GetOrDefault(Guid.Parse(entry["StudentID"]))?.EdvalClasses.Iteri((s, ind) =>
                {
                    maxClasses = Math.Max(maxClasses, ind);
                    entry[$"EdvalClass{ind + 1}"] = s;
                });
            });

            foreach (var entry in studentsDict)
            {
                for (int i = 0; i < maxClasses; i++)
                {
                    var classField = $"Class{i + 1}";
                    if (!entry.ContainsKey(classField))
                        entry[classField] = "";

                    var edvalClassField = $"EdvalClass{i + 1}";
                    if (!entry.ContainsKey(edvalClassField))
                        entry[edvalClassField] = "";
                }
            }
        }

        private static void RenameField(
            Dictionary<string, string> entry,
            string source,
            string target)
        {
            RenameField(entry, source, target, "");
        }

        private static void RenameField<TK, TV>(
            Dictionary<TK, TV> entry,
            TK source,
            TK target,
            TV defValue = default(TV))
        {
            entry[target] = entry.GetOrDefault(source, defValue);
            entry.Remove(source);
        }

        private static void EnrichPeriods(Tdf9Content content)
        {
            var daysDict = content.GetOrNull("Days");
            var periods = content.GetOrNull("Periods");
            if (periods == null || daysDict == null) return;
            var days = daysDict.ToDictionary(x => Guid.Parse(x["DayID"]));
            foreach (var entry in periods)
            {
                LookReference(entry, days, "DayID", "Day", "Code");
                entry["PeriodCode"] = entry.GetOrDefault("Code");
                entry.Remove("Code");
            }
        }

        private static IEnumerable<string> NonEmptyUnique(IEnumerable<string> values)
        {
            return values.Where(x => x != "").Select(x => x).Distinct();
        }

        public sealed class ClassInTimetable
        {
            public readonly Guid ClassId;
            public readonly string Teacher;
            public readonly string Year;

            public ClassInTimetable(Guid classId, string teacher, string year)
            {
                ClassId = classId;
                Teacher = teacher;
                Year = year;
            }
        }

        private static void EnrichEdvalClassCodes(Tdf9Content content)
        {
            content.Classes.Iter(x => x["EdvalClassCode"] = FormatEdvalClassCode(x));
        }

        private static void EnrichClasses(Tdf9Content content)
        {
            var facultiesDict = content.GetOrNull("Faculties");
            var classes = content.GetOrNull("Classes");
            var timetableDict = content.GetOrNull("Timetables");

            if (classes == null) return;

            foreach (var entry in classes)
            {
                var suffix = entry.GetOrDefault("Suffix", "");
                entry.Remove("Suffix");
                entry["ClassGUID"] = entry.GetOrDefault("ClassID", "");
                entry["ClassID"] = suffix.Trim();

                var subjectCode = entry.GetOrDefault("SubjectCode", "");
                entry.Remove("SubjectCode");
                entry["CourseCode"] = subjectCode;

                RenameField(entry, "Name", ".Name");

            }

            if (facultiesDict == null || timetableDict == null) return;

            var faculties = facultiesDict.ToDictionary(x => Guid.Parse(x["FacultyID"]));

            var timetable = timetableDict
                .Where(x => x["ClassID"] != "")
                .Select(x => new ClassInTimetable(
                    Guid.Parse(x["ClassID"]),
                    x.GetOrDefault("Teacher", ""),
                    x.GetOrDefault("Year", "")))
                .GroupBy(x => x.ClassId)
                .ToDictionary(x => x.Key, x => x.ToArray());

            foreach (var entry in classes)
            {
                LookReference(entry, faculties, "FacultyID", "Faculty", "Code");

                LookupTimetables(timetable, entry);
            }
        }

        private static string FormatEdvalClassCode(Dictionary<string, string> entry)
        {
            var year = entry.GetOrDefault("Year", "").TrimStart('0');
            var courseCode = entry.GetOrDefault("CourseCode", "").TrimStart('0');

            if (courseCode.StartsWith(year)) year = "";

            var classId = entry.GetOrDefault("ClassID", "").TrimStart('0');
            return $"{year.Trim()}{courseCode.Trim()}.{classId.Trim()}";
        }

        private static void LookupTimetables(Dictionary<Guid, ClassInTimetable[]> timetable,
            Dictionary<string, string> entry)
        {
            var @class = timetable.GetOrDefault(Guid.Parse(entry["ClassGUID"]));
            if (@class == null) return;

            entry["#Per"] = @class.Length.ToString();
            entry["Year"] = string.Join(",", NonEmptyUnique(@class.Select(x => x.Year)));
            entry["TeacherPref"] = string.Join(",", NonEmptyUnique(@class.Select(x => x.Teacher)));
        }

        private static void EnrichRollClasses(Tdf9Content content)
        {
            var yearLevelsDict = content.GetOrNull("YearLevels");
            var rollClasses = content.GetOrNull("RollClasses");
            if (rollClasses == null || yearLevelsDict == null) return;
            var yearLevels = yearLevelsDict.ToDictionary(x => Guid.Parse(x["YearLevelID"]));
            foreach (var entry in rollClasses)
            {
                LookReference(entry, yearLevels, "YearLevelID", "Year", "Code");
            }
        }

        private static void EnrichTeachers(Tdf9Content content)
        {
            var teachers = content.GetOrNull("Teachers");
            if (teachers == null) return;

            foreach (var entry in teachers)
            {
                var lastName = entry.GetOrDefault("LastName", "").ToUpper();
                var salutation = entry.GetOrDefault("Salutation", "");
                var firstName = entry.GetOrDefault("FirstName", "").ToTitleCase();
                entry["FullName"] = $"{lastName}, {firstName} {salutation}".Trim();

                int daysUnavailable;
                if (int.TryParse(entry.GetOrDefault("DaysUnavailable"), out daysUnavailable) && daysUnavailable > 0)
                {
                    entry["Unavailable"] = $"{daysUnavailable}daysoff";
                }
                else
                {
                    entry["Unavailable"] = "";
                }

                RenameField(entry, "FirstName", ".FirstName");
                RenameField(entry, "LastName", ".Surname");
                RenameField(entry, "ProposedLoad", "MaxLoad");
                DivideBy100(entry, "MaxLoad");
                DivideBy100(entry, "ActualLoad");
                DivideBy100(entry, "FinalLoad");
            }
        }

        private static void EnrichTimetable(Tdf9Content content)
        {
            var timetable = content.GetOrNull("Timetables");
            var rollClassesDict = content.GetOrNull("RollClasses");
            var periodsDict = content.GetOrNull("Periods");
            var classesDict = content.GetOrNull("Classes");
            var roomsDict = content.GetOrNull("Rooms");
            var teachersDict = content.GetOrNull("Teachers");

            if (new object[] { timetable, rollClassesDict, periodsDict }.Any(x => x == null)) return;

            var rollClasses = ToDictionary(rollClassesDict, x => Guid.Parse(x["RollClassID"]));
            var periods = ToDictionary(periodsDict, x => Guid.Parse(x["PeriodID"]));
            var classes = ToDictionary(classesDict, x => Guid.Parse(x["ClassID"]));
            var rooms = ToDictionary(roomsDict, x => Guid.Parse(x["RoomID"]));
            var teachers = ToDictionary(teachersDict, x => Guid.Parse(x["TeacherID"]));

            foreach (var entry in timetable)
            {
                LookReference(entry, rollClasses, "RollClassID", "Year", "Year");

                LookReference(entry, periods, "PeriodID", "Period", "PeriodCode");
                LookReference(entry, periods, "PeriodID", "Day", "Day");

                LookReference(entry, classes, "ClassID", "ClassCode", "Code");

                LookReference(entry, rooms, "RoomID", "Room", "Code");
                LookReference(entry, teachers, "TeacherID", "Teacher", "Code");

                LookReference(entry, teachers, "TeacherID", "Teacher", "Code");
            }
        }

        private static void LookReference(
            Dictionary<string, string> entry,
            Dictionary<Guid, Dictionary<string, string>> referencedCollection,
            string referenceFieldId,
            string appendAs,
            string referenceField)
        {
            Guid id;

            entry[appendAs] = !Guid.TryParse(entry.GetOrDefault(referenceFieldId, ""), out id)
                ? ""
                : referencedCollection
                    .GetOrDefault(id, new Dictionary<string, string>())
                    .GetOrDefault(referenceField, "");
        }

        private static void WriteCollection(
            string collectionName, 
            Dictionary<string, string>[] entries, 
            Utils.OutDirs outDirs)
        {
            if (!entries.Any()) return;

            if (collectionName == "Timetables")
            {
                using (var file = File.CreateText(Path.Combine(outDirs.LessCommonFiles, "Timetable GUIDs.csv")))
                {
                    var fields = SortKeys(collectionName, entries[0].Keys)
                        .Where(x => x.EndsWith("ID"))
                        .ToArray();

                    WriteCollection(entries, file, fields);
                }

                using (var file = File.CreateText(Path.Combine(outDirs.MainDir, "TTable.csv")))
                {
                    var fields = SortKeys(collectionName, entries[0].Keys)
                        .Where(x => !x.EndsWith("ID") && x != "EdvalClassCode")
                        .ToArray();

                    WriteCollection(entries, file, fields);
                }

                using (var file = File.CreateText(Path.Combine(outDirs.MainDir, "TTableEdvalClassCodes.csv")))
                {
                    var fields = SortKeys(collectionName, entries[0].Keys)
                        .Where(x => !x.EndsWith("ID") && x != "EdvalClassCode")
                        .ToArray();

                    entries.Iter(x => RenameField(x, "EdvalClassCode", "ClassCode"));

                    WriteCollection(entries, file, fields);
                }

            }
            if (collectionName == "Students")
            {
                using (var file = File.CreateText(Path.Combine(outDirs.MainDir, "Students.csv")))
                {
                    var fields = SortKeys(collectionName, entries[0].Keys)
                        .Where(x => !x.StartsWith("EdvalClass"))
                        .ToArray();

                    WriteCollection(entries, file, fields);
                }

                using (var file = File.CreateText(Path.Combine(outDirs.MainDir, "StudentsEdvalClassCodes.csv")))
                {
                    var fields = SortKeys(collectionName, entries[0].Keys)
                        .Where(x => !x.StartsWith("EdvalClass"))
                        .ToArray();

                    entries.Iter(entry =>
                    {
                        var edvalClasses = entry.Keys.Where(key => key.StartsWith("EdvalClass")).ToArray();

                        edvalClasses.Iter(key =>
                        {
                            if (key.StartsWith("EdvalClass"))
                            {
                                RenameField(entry, key, key.Substring(5));
                            }
                        });
                    });

                    WriteCollection(entries, file, fields);
                }
            }
            else
            {
                var lessCommonNames = new[]
                {

                    "AdjacentAllocations",
                    "AdjacentAllocationGroups",
                    "Allocations",
                    "BlockingAllocations",
                    "ClassGroups",
                    "Courses",
                    "GroupPeriodUnavailables",
                    "MeetingAllocations",
                    "MeetingPreallocations",
                    "Meetings",
                    "MeetingTeachers",
                    "MeetingUnavailables",
                    "MRCGClassGroups",
                    "MRCGs",
                    "PublishedTimetables",
                    "RollClasses",
                    "RURReferences",
                    "RURRooms",
                    "RURs",
                    "StudentFiles",
                    "TeacherYardDutyUnavailables",
                    "Timetable GUIDs",
                    "TimetableEdits",
                    "YardDuties",
                    "YardDutyAreas",
                    "YardDutySessions",
                    "YardDutyAreaUnavailables"
                };

                var lessCommon = new HashSet<string>(lessCommonNames.Select(x => x.ToLower()));

                var outDir = lessCommon.Contains(collectionName.ToLower()) ? outDirs.LessCommonFiles : outDirs.MainDir;

                if (entries != null && entries.Any())
                {
                    using (var file = File.CreateText(Path.Combine(outDir, $"{collectionName}.csv")))
                    {
                        var fields = SortKeys(collectionName, entries[0].Keys)
                            .ToArray(); // new SortedSet<string>(entries.SelectMany(x => x.Keys));

                        WriteCollection(entries, file, fields);
                    }
                }
            }
        }

        private static void WriteCollection(Dictionary<string, string>[] entries, StreamWriter file, string[] fields)
        {
            file.WriteLine(string.Join(",", fields));

            foreach (var entry in entries)
            {
                var values = fields
                    .Select(f => entry.ContainsKey(f) ? entry[f] : "")
                    .Select(Utils.EscapeCsvValue);

                file.WriteLine(string.Join(",", values));
            }
        }

        private static IEnumerable<string> SortKeys(string collectionName, IEnumerable<string> keys)
        {
            var sorts = new Dictionary<string, string>
            {
                { "ConstraintOptions", "SubjectCode,SubjectName,AlternateName,AlternateCode,GroupNo,TypeOf,Limit,Note,ConstraintID,OptionID" },
                { "Options", "SubjectCode,SubjectName,SubgridNo,NoLessons,NoLines,NoTeachers,AutoCreate,AlternateName," +
                             "AlternateCode,OptionID,SubjectID" },
                { "Preferences", "Year,StudentID,Student Full Name,OptionName,OptionCode,Class,Preference #," +
                                 "OptionID,ClassID" },
                { "Timetables", "Day,Year,Period,ClassCode,Teacher,Room" },
                { "Students", "YearLevel,StudentCode,FullName,Gender,Email,House,GroupLetter,HomeGroup,Class1,Class2," +
                              "Class3,Class4,Class5,Class6,Class7,Class8,Class9,Class10,Class11,Class12,Class13," +
                              "Class14,Class15,Class16,Class17,Class18,Class19,Class20,Class21,Class22,Class23," +
                              "Class24,Class25,Class26,Class27,Class28,Class29,Class30,.FirstName,.PreferredName," +
                              "MiddleName,.Surname,WCSet,BOSCode,SpareField1,SpareField2,SpareField3,StudentGUID" },
                { "StudentLessons", "Year,StudentCode,StudentName,ClassCode,EdvalClassCode,RollClassCode,LessonType," +
                                    "StudentGUID" },
                { "YearLevels", "Code" },
                { "Allowances", "Code,Allowance,Teacher,Load,UnscheduledDutyID,TeacherID" },
                { "Meetings", "Year,ClassCode,Name,#Per,TeacherPref,MeetingCode,Load" },
                { "MeetingTeachers", "Teacher,Meeting" },
                { "RoomUnavailables", "Room,PeriodID,Reason" },
                { "TeacherUnavailables", "Teacher,PeriodID,Reason,Unavailable" },
                { "Periods", "Day,PeriodCode,Name,Doubles,Triples,Quadruples,SiteMove,Load,Index,Number,StartTime," +
                             "FinishTime,PeriodID,DayID" },
                { "Days", "Code,Name" },
                { "Faculties", "Code,Name,FacultyGroupID,LoadLimit" },
                { "FacultyTeachers", "Teacher,Faculty,LoadLimitMin,LoadLimitMax" },
                { "Teachers", "Code,FullName,MaxLoad,Faculty,Unavailable,Email,ActualLoad,FinalLoad,.FirstName," +
                              "MiddleName,.Surname,Salutation,DaysUnavailable,PeriodsUnavailable,ConsecutivePeriods," +
                              "MaxYardDutyLoad,PeriodsOff,SpareField1,SpareField2,SpareField3,TeacherID" },
                { "YardDuties", "YardDutyArea,Period,Teacher,Load,YardDutySession,YardDutyAreaID,YardDutySessionID," +
                                "TeacherID,YardDutyID" },
                { "YardDutyAreas", "Code,Name,Campus" },
                { "Rooms", "Code,Name,Size,Campus,Notes,Unavailable" },
                { "Classes", "Year,CourseCode,ClassID,Code,EdvalClassCode,Faculty,SubjectName,#Per,Spread,TeacherPre," +
                             ".Name,BOSClassCode1,BOSClassCode2,BOSClassCode3,ClassCodeCheck,FacultyID,ClassCodeGUID" }
            };

            if (sorts.ContainsKey(collectionName))
            {
                return SortFields(keys, sorts[collectionName].Split(','));
            }

            return keys;
        }

        private static IEnumerable<string> SortFields(IEnumerable<string> keys, string[] fields)
        {
            var originalKeys = keys.ToArray();
            var originalSet = new HashSet<string>(originalKeys);

            var expectedFields = new HashSet<string>(fields);

            foreach (var field in fields)
            {
                if (originalSet.Contains(field)) yield return field;
            }

            foreach (var field in originalKeys)
            {
                if (!expectedFields.Contains(field)) yield return field;
            }
        }

        private static IEnumerable<Dictionary<string, string>> ParseCollection(XElement collection)
        {
            if (!collection.HasElements) yield break;

            foreach (var entryNode in collection.Elements())
                yield return entryNode.Elements().ToDictionary(x => x.Name.LocalName, x => x.Value);
        }
    }
}