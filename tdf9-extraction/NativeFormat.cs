using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;

namespace Tdf9Extraction
{
    public static class NativeFormat
    {
        static readonly Func<Tdf9Content, IEnumerable<string>>[] Renderers =
        {
            GetHeader,
            Section("Periods:", 7, new[]
            {
                Group("[Days]", RenderDays),
                Group("[PeriodNums]", RenderPeriodNums),
                Group("[Periods]", RenderPeriods),
                Group("[GroupedTimes]", RenderGroupedTimes)
            }),
            Section("Teachers", 13, new[]
            {
                Group("\r\nFaculties:", RenderFaculties),
                Group("", RenderTeachers),
                Group("Sets:", RenderTeacherSets)
            }),
            Section("Rooms", 13, new[]
            {
                Group("", RenderRooms),
                Group("Sets:", RenderRoomSets)
            }),
            Section("Classes:", 9, new Func<Tdf9Content, IEnumerable<string>>[] {RenderClasses}),
            GetYearLevels,
            RenderElectiveThings,
            EmptyLines(2),
            Section("Allowances:", 14, new Func<Tdf9Content, IEnumerable<string>>[] {RenderAllowances}),
            EmptyLines(2),
            Section("SportingHouses:", 14, new Func<Tdf9Content, IEnumerable<string>>[] {RenderSportingHouses}),
            EmptyLines(2),
            Section("Parameters:", 14, new Func<Tdf9Content, IEnumerable<string>>[] {RenderParameters}),
            RenderEof
        };

        private static Func<Tdf9Content, IEnumerable<string>> EmptyLines(int count)
        {
            return _ => Enumerable.Repeat(string.Empty, count);
        }

        private static IEnumerable<string> RenderEof(Tdf9Content content)
        {
            yield return "";
            yield return "";
            yield return "ThatsAllFolks";
        }

        public static IEnumerable<string> RenderElectiveThings(Tdf9Content content)
        {
            return content.Electives.SelectMany(
                el => RenderElectiveThing(el, content.Students.GroupById("StudentGUID")));
        }

        public static IEnumerable<string> RenderElectiveThing(
            Tdf9Content content,
            Dictionary<Guid, Dictionary<string, string>> studentCodes)
        {
            var year = GetElectiveYear(content);

            if (year == null) yield break;
            yield return $"ElectiveThing {year}_elecs:";
            yield return new string('~', 18);
            yield return $"LN={year}_elecs TY=Electives YY={year}";
            yield return string.Empty;

            foreach (var line in RenderElectiveCources(content, year))
            {
                yield return line;
            }

            yield return string.Empty;

            foreach (var line in RenderYearFacets(year))
            {
                yield return line;
            }

            yield return string.Empty;

            foreach (var line in RenderStudentFacets(content, studentCodes))
            {
                yield return line;
            }

            yield return string.Empty;
        }

        private static IEnumerable<string> RenderYearFacets(string year)
        {
            yield return "[YearFacets]";
            yield return $"{year} LN={year} MU=10 XU=14 MO=0 RO=3 MR=6 NS=N SO=N TOPTEXT=<![CDATA[";
            yield return "";
            yield return "]]>";
            yield return "RIGHTTEXT=<![CDATA[";
            yield return "";
            yield return "]]>";
            yield return "SUCCESSTEXT=<![CDATA[";
            yield return "Please get form signed, and return to school.<br /><br /><br />";
            yield return "Signature: ____________________ <br /> Parent / Carer<br />";
            yield return "]]>";
            yield return "E1=N E2=N EN=N DC=. NL=12";
        }

        private static IEnumerable<string> RenderStudentFacets(
            Tdf9Content content, 
            Dictionary<Guid, Dictionary<string, string>> studentCodes)
        {
            yield return "[StudentFacets]";

            var preferences = content.GetOrCreate("Preferences")
                .GroupBy(x => Guid.Parse(x["StudentID"]))
                .ToDictionary(
                    x => x.Key, 
                    x => x.OrderBy(p => int.Parse(p["Preference #"])).ToArray());
            
            foreach (var student in content.Students)
            {
                var studentId = Guid.Parse(student["StudentGUID"]);

                if (studentCodes.ContainsKey(studentId))
                {
                    var studentPrefs = preferences.GetOrDefault(studentId) ?? new Dictionary<string, string>[0];

                    var prefStr = string.Join(
                        " ",
                        studentPrefs.Select((x, ind) => $"P{Alphabet[ind]}={x["OptionCode"].NormalizeCode()}"));

                    yield return $"{studentCodes[studentId]["StudentCode"].NormalizeCode()} PR={studentPrefs.Length} " +
                                 $"{prefStr}";
                }
            }
        }

        private static IEnumerable<string> RenderElectiveCources(Tdf9Content content, string year)
        {
            yield return "[Courses]";

            var subjects =
                content.GetOrCreate("Subjects")
                   .GroupById("SubjectID");

            foreach (var option in content.GetOrCreate("Options"))
            {
                var subject = subjects[Guid.Parse(option["SubjectID"])];
                
                int classSize;
                if (int.TryParse(subject["ClassSizeMaximum"], out classSize))
                {
                    classSize = classSize == 0 ? 24 : classSize;
                }
                else
                {
                    classSize = 24;
                }

                yield return $"{option["SubjectCode"].NormalizeCode()} NU={option["NoLessons"]} " +
                             $"UN={subject["Units"]} TE=. RO=. MS={classSize} SU=\"{subject["SubjectName"]}\" " +
                             $"YR={year}";
            }

        }

        private static string GetElectiveYear(Tdf9Content content)
        {
            var students = content.Students;
            
            var years =
                students
                    .GroupBy(x => x["YearLevel"])
                    .Where(x => !string.IsNullOrWhiteSpace(x.Key));

            try
            {
                return years.SingleOrDefault()?.Key;
            }
            catch (InvalidOperationException)
            {
                return null;
            }
        }

        private static IEnumerable<string> RenderSportingHouses(Tdf9Content content)
        {
            return content.Students.Select(x => x.GetOrDefault("House"))
                .Where(x => !string.IsNullOrWhiteSpace(x))
                .Distinct()
                .Select(h => $"{CleanCode(h)} LN=. CO=0xffffff");
        }

        public static string CleanCode(string code)
        {
            return Regex.IsMatch(code, @"[^\w\d\-\.]") ? $"\"{code}\"" : code;
        }

        public static IEnumerable<string> RenderAllowances(Tdf9Content content)
        {
            return content.Allowances
                .Select(x => $"{CleanCode(x["Code"])} LN=\"{x["Allowance"]}\" LD={x["Load"]}" +
                             GetTeacher(x));
        }

        private static string GetTeacher(IReadOnlyDictionary<string, string> x)
        {
            if (!x.ContainsKey("Teacher") || string.IsNullOrWhiteSpace(x["Teacher"])) return "";
            return $" TE={x["Teacher"]}";
        }
        
        const string Alphabet = "123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";

        private static IEnumerable<string> GetYearLevels(Tdf9Content content)
        {
            var classes = Enumerable.Range(0, 30).Select(x => $"EdvalClass{x}").ToArray();

            foreach (var yearLevel in content.Students.GroupBy(x => x["YearLevel"]))
            {
                yield return $"YearLevel {yearLevel.Key}:";
                yield return new string('~', 10);
                yield return " QP=n YW=0";
                yield return "";
                yield return "[Students]";

                foreach (var s in yearLevel)
                {
                    var studentClasses = classes
                        .Where(cl => s.ContainsKey(cl))
                        .Select(cl => s[cl])
                        .Where(x => !string.IsNullOrWhiteSpace(x))
                        .Select((cl, i) => $"Z{Alphabet[i]}={cl}");

                    var email = ".";
                    if (!string.IsNullOrWhiteSpace(s.GetOrDefault("Email"))) email = s.GetOrDefault("Email");

                    yield return $"{s["StudentCode"]} LN=\"{s["FullName"]}\" MF={s["Gender"]} HO=\"{s["House"]}\" " +
                                 $"GU={Guid.Parse(s["StudentGUID"]):D} {String.Join(" ", studentClasses)} EM={email}";
                }

                yield return "";
            }
        }

        public static IEnumerable<string> RenderClasses(Tdf9Content content)
        {
            var byClass = content.Timetables
                .Where(x => !string.IsNullOrEmpty(x.GetOrDefault("ClassID")))
                .GroupBy(x => Guid.Parse(x["ClassID"]))
                .Select(x => new
                {
                    x.Key,
                    Periods = x
                        .Select(t => new
                        {
                            Teacher = t.GetOrDefault("Teacher"),
                            Time = GetPeriodFullCode(t["Day"], t["Period"]),
                            Room = string.IsNullOrWhiteSpace(t.GetOrDefault("Room")) ? "." : t.GetOrDefault("Room")
                        })
                })
                .ToDictionary(x => x.Key, x => x.Periods);

            return content.Classes
                .SelectMany(x =>
                {
                    var subjectName = x.GetOrDefault("SubjectName");
                    var per = x.GetOrDefault("#Per", "0");
                    var spread = x.GetOrDefault("Spread");
                    var faculty = x.GetOrDefault("Faculty");
                    var classGuid = Guid.Parse(x["ClassGUID"]);
                    var teacher = x.GetOrDefault("TeacherPref", "");
                    var classCode = CleanCode(x["EdvalClassCode"]);
                    var classRow =
                        $"{classCode} YR={x["Year"]} TE=\"{teacher}\" RO=. SU=\"{subjectName}\" " +
                        $"CO=0xffffff NP={per} NI={per} LO={per} TP={spread} 3B=0 FA=\"{faculty}\" GU={classGuid:D}";

                    if (!byClass.ContainsKey(classGuid)) return new string[0];

                    var periodsRow = string.Join(
                        " ",
                        byClass.GetOrDefault(classGuid)
                            .Select(p =>
                            {
                                var tags = new[]
                                {
                                    new[] {"TE", p.Teacher},
                                    new[] {"TI", p.Time},
                                    new[] {"RO", p.Room},
                                };

                                var periods = tags
                                    .Where(t => !string.IsNullOrWhiteSpace(t[1]))
                                    .Select(t => $"{t[0]}={CleanCode(t[1])}");

                                return $"[{string.Join(" ", periods)}]";
                            }));

                    return new[] {classRow, $"\t{periodsRow}"};
                });
        }

        private static IEnumerable<string> RenderRoomSets(Tdf9Content arg)
        {
            yield break;
        }

        public static IEnumerable<string> RenderRooms(Tdf9Content content)
        {
            return content.GetOrCreate("Rooms")
                .Select(x => $"{CleanCode(x["Code"])} CA={x["Size"]} NB=\"{x["Notes"]}\" " +
                             $"GU=\"{Guid.Parse(x["RoomID"]).ToString("B").ToUpper()}\"");
        }

        private static readonly string[] SkipFaculties = {"part time", "tef", "", null};

        public static IEnumerable<string> RenderTeachers(Tdf9Content content)
        {
            var facultyWeights = CalculateFacultyWeights(content);

            return content.Teachers.Select(
                t =>
                {
                    var classFaculties = content.GetOrCreate("Classes")
                        .Where(c => !SkipFaculties.Contains(c["Faculty"].ToLower()))
                        .GroupBy(c => Guid.Parse(c["ClassGUID"]))
                        .ToDictionary(c => c.Key, c => c.First()["Faculty"]);

                    var mainClass = content.Timetables
                        .Where(tt => tt["Teacher"] == t["Code"])
                        .Select(tt => Guid.Parse(tt["ClassID"]))
                        .GroupBy(x => x)
                        .OrderByDescending(x => x.Count())
                        .Select(x => x.Key)
                        .FirstOrDefault();

                    var faculty = classFaculties.ContainsKey(mainClass)
                        ? classFaculties[mainClass]
                        : GetFacultyWithMaxWeight(facultyWeights, t["Faculty"]);

                    return $"{CleanCode(t["Code"])} LN=\"{t["FullName"]}\" ML={t["MaxLoad"]} PM=0 FA=\"{faculty}\" " +
                           $"UA=\"{t["Unavailable"]}\" EM=\"{t["Email"]}\" FT=1 GU={Guid.Parse(t["TeacherID"]):D}";
                });
        }

        private static string GetFacultyWithMaxWeight(Dictionary<string, int> facultyWeights, string faculty)
        {
            return faculty
                       .Split(',')
                       .Where(x => facultyWeights.Keys.Contains(x.ToLower()))
                       .Where(x => !SkipFaculties.Contains(x.ToLower()))
                       .OrderByDescending(x => facultyWeights[x.ToLower()])
                       .FirstOrDefault() ?? "";
        }

        public static Dictionary<string, int> CalculateFacultyWeights(Tdf9Content content)
        {
            var classFaculties = content.Classes
                .Where(x => x.ContainsKey("Faculty") && !string.IsNullOrWhiteSpace(x["Faculty"]))
                .GroupBy(x => x["ClassGUID"])
                .ToDictionary(x => x.Key, x => x.First()["Faculty"]);

            return content
                .Timetables
                .Select(x => new {Row = x, Faculty = classFaculties.GetOrDefault(x["ClassID"])})
                .Where(x => !string.IsNullOrWhiteSpace(x.Faculty) && !string.IsNullOrWhiteSpace(x.Row["TeacherID"]))
                .GroupBy(x => x.Faculty)
                .OrderBy(g => g.Select(x => x.Row["ClassID"]).Distinct().Count())
                .Select((g, index) => new {Weight = g.Count(), Faculty = g.Key})
                .ToDictionary(x => x.Faculty.ToLower(), x => x.Weight);
        }

        public static IEnumerable<string> RenderFaculties(Tdf9Content content)
        {
            var classesFaculties = new HashSet<string>(content.Classes.Select(x => x["Faculty"]));
            var teacherFaculties = new HashSet<string>(
                content.GetOrCreate("FacultyTeachers").Select(x => x["Faculty"])); 
            
            return classesFaculties.Intersect(teacherFaculties).Select(x => $"{CleanCode(x)}\tCO=0xffffff");
        }

        private static IEnumerable<string> RenderGroupedTimes(Tdf9Content content)
        {
            yield return "{}";
        }

        public static IEnumerable<string> Render(Tdf9Content content)
        {
            foreach (var r in Renderers)
            {
                foreach (var str in r(content))
                {
                    yield return str;
                }

                yield return string.Empty;
            }
        }


        public static void Export(Utils.OutDirs dirs, Tdf9Content content)
        {
            var outputFile = dirs.NativeFileName;

            using (var file = File.CreateText(outputFile))
            {
                foreach (var s in Render(content))
                {
                    file.WriteLine(s);
                }
            }
        }

        public static IEnumerable<string> RenderPeriods(Tdf9Content content)
        {
            return content.Periods
                .Select(x =>
                {
                    var times = $"{x["StartTime"].TrimTime()}-{x["FinishTime"].TrimTime()}";

                    return new
                    {
                        DayCode = x["Day"],
                        PeriodCode = x["PeriodCode"],
                        Code = GetPeriodFullCode(x["Day"], x["PeriodCode"]),
                        PeriodType = x["PeriodCode"].IsNonNegativeInteger() ? "T" : "O",
                        Times = times == "-" ? "0:00" : times
                    };
                })
                .Select(x => $"{x.Code} TY={x.PeriodType} " +
                             $"MI={x.Times} DA={x.DayCode} PE={x.PeriodCode}");
        }

        public static string GetPeriodFullCode(string day, string periodCode)
        {
            var divider = periodCode.IsNonNegativeInteger() ? "-" : "";
            return $"{day}{divider}{periodCode}";
        }

        public static IEnumerable<string> RenderDays(Tdf9Content content)
        {
            return content.Days
                .Select(x => $"{CleanCode(x["Code"])} TY=T MI=0:00");
        }

        private static Func<Tdf9Content, IEnumerable<string>> Section(
            string title,
            int length,
            IEnumerable<Func<Tdf9Content, IEnumerable<string>>> sections)
        {
            return content =>
                title.Yield()
                    .Concat(new string('~', length).Yield())
                    .Concat(sections.SelectMany(s => s(content)));
        }

        private static Func<Tdf9Content, IEnumerable<string>> Group(
            string title,
            Func<Tdf9Content, IEnumerable<string>> content)
        {
            return c => title.Yield().Concat(content(c)).Concat(string.Empty.Yield());
        }

        public static IEnumerable<string> GetHeader(Tdf9Content content)
        {
            yield return $"Version 9.9.999.9";
        }

        public static IEnumerable<string> RenderPeriodNums(Tdf9Content content)
        {
            return content.Periods
                .GroupBy(x => x["PeriodCode"])
                .Select(g => new
                {
                    Code = NormalizePeriodCode(g.Key),
                    Times = g.Select(x => $"{x["StartTime"].TrimTime()}-{x["FinishTime"].TrimTime()}")
                        .Where(x => x != "-"),
                    PeriodType = g.Key.IsNonNegativeInteger() ? "T" : "O"
                })
                .Select(x => $"{CleanCode(x.Code)} TY={x.PeriodType} MI={x.Times.FirstOrDefault() ?? "0:00"}");
        }


        public static IEnumerable<string> RenderTeacherSets(Tdf9Content content)
        {
            yield break;
        }

        public static string NormalizePeriodCode(string input)
        {
            var parts = input.Split('-');
            if (parts.Length != 2) return input;

            if (parts[1].IsNonNegativeInteger()) return input;

            return string.Join("", parts);
        }

        public static IEnumerable<string> RenderParameters(Tdf9Content content)
        {
            var year = DateTime.UtcNow.Year.ToString();
            var yearLevel = content.GetOrNull("YearLevels")?.FirstOrDefault()?["Code"] ?? ".";
            var faculty = content.Teachers
                .GroupBy(x => x["Faculty"])
                .Where(x => !string.IsNullOrEmpty(x.Key))
                .OrderByDescending(x => x.Count())
                .FirstOrDefault();
            yield return $" YY={year} YR={yearLevel} FA={faculty?.Key ?? "."} ET=11_elecs";
        }
    }

    public static class Helpers
    {
        public static bool IsNonNegativeInteger(this string str)
        {
            int val;
            return int.TryParse(str, out val) && val >= 0;
        }
        
        public static string TrimTime(this string str)
        {
            DateTime val;
            return !DateTime.TryParse(str, out val) ? str : val.ToString("H:mm");
        }
        
        public static bool IsNonEmptyValue(this Dictionary<string, string> arg, string fieldName)
        {
            return arg.ContainsKey(fieldName) && !string.IsNullOrWhiteSpace(arg[fieldName]);
        }

        public static string NormalizeCode(this string code)
        {
            return Regex.Replace(code, @"[\(\)\s]", "").Replace("/", "-").Replace("&", "-");
        }
    }
}