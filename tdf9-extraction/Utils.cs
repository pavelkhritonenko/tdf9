﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Tdf9Extraction
{
    public static class Utils
    {

        public static Dictionary<Guid, Dictionary<string, string>> GroupById(
            this Dictionary<string, string>[] dict,
            string idColumn)
        {
            return dict.ToDictionary(x => Guid.Parse(x[idColumn]), x => x);
        }

        public static string ToTitleCase(this string str)
        {
            var ci = System.Globalization.CultureInfo.CurrentCulture;
            return ci.TextInfo.ToTitleCase(str.ToLower(ci));
        }

        public static IEnumerable<string> ReadLines(this StreamReader reader)
        {
            while (!reader.EndOfStream)
            {
                yield return reader.ReadLine();
            }
        }

        public static string EscapeCsvValue(this string str)
        {
            if (!str.Contains("\"") && !str.Contains(",")) return str;
            return $"\"{str.Replace("\"", "\"\"")}\"";
        }


        public static IEnumerable<T> Yield<T>(this T value)
        {
            yield return value;
        }

        public sealed class OutDirs
        {
            public readonly string MainDir;
            public readonly string LessCommonFiles;
            public readonly string NativeFileName;

            public OutDirs(string mainDir, string lessCommonFiles, string nativeFileName)
            {
                MainDir = mainDir;
                LessCommonFiles = lessCommonFiles;
                NativeFileName = nativeFileName;
            }

            public OutDirs CreatePreferencesFolder(string fileName)
            {
                var pref = Path.Combine(MainDir, fileName);
                var lessCommonFiles = Path.Combine(pref, "Less common files");

                Directory.CreateDirectory(pref);
                Directory.CreateDirectory(lessCommonFiles);

                return new OutDirs(pref, lessCommonFiles, NativeFileName);
            }
        }

        public static OutDirs CreateOutputDir(string sourceFileName, string outDir = null, string subDir = null)
        {
            var fileName = Path.GetFileNameWithoutExtension(sourceFileName);

            outDir = outDir ?? Path.GetDirectoryName(sourceFileName) ?? ".";

            var availableDir = new[] { "" }
                .Concat(Enumerable.Range(1, int.MaxValue).Select(n => $"-{n}"))
                .Select(suffix => Path.Combine(outDir, $"{fileName}{suffix}"))
                .First(d => !Directory.Exists(d) && !File.Exists(d));

            var withSubDir = subDir == null ? availableDir : Path.Combine(availableDir, subDir);

            var fullName = Directory.CreateDirectory(withSubDir).FullName;
            
            return new OutDirs(
                fullName,
                Directory.CreateDirectory(Path.Combine(withSubDir, "Less common files")).FullName,
                Path.Combine(fullName, $"{fileName} (converted).ett"));
        }

        public static IEnumerable<TResult> Pairwise<TSource, TResult>(
            this IEnumerable<TSource> source,
            Func<TSource, TSource, TResult> resultSelector)
        {
            var previous = default(TSource);

            using (var it = source.GetEnumerator())
            {
                if (it.MoveNext())
                    previous = it.Current;

                while (it.MoveNext())
                    yield return resultSelector(previous, previous = it.Current);
            }
        }

        public static void Iter<T>(this IEnumerable<T> source, Action<T> a)
        {
            foreach (var item in source) a(item);
        }

        public static void Iteri<T>(this IEnumerable<T> source, Action<T, int> a)
        {
            int i = 0;
            foreach (var item in source) a(item, i++);
        }

        public static TV GetOrDefault<TV, TK>(this Dictionary<TK, TV> dict, TK name, TV defaultValue = default(TV))
        {
            return !dict.ContainsKey(name) ? defaultValue : dict[name];
        }
    }
}