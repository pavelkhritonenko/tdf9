﻿using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Tdf9Extraction
{
    internal static class Container
    {
        public static bool IsCasFormat(IEnumerable<string> entries)
        {
            var names = new HashSet<string>(entries
                .Select(x => Path.GetFileName(x)?.ToLower())
                .Where(x => x != null));

            var expectedFiles = new HashSet<string>(new[]
            {
                "classes.txt",
                "days.txt",
                "row.txt",
                "schedule.txt",
                "scw.txt"
            });

            return expectedFiles.IsSubsetOf(names);
        }
    }
}