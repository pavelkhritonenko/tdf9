﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using Microsoft.VisualBasic.FileIO;
using Tdf9Extraction.Recognizer;
using static System.String;

namespace Tdf9Extraction
{
    public static class CasParser
    {
        public static void Extract(ICasSource casSource, string outDir)
        {
            if (casSource == null) throw new ArgumentNullException(nameof(casSource));
            if (outDir == null) throw new ArgumentNullException(nameof(outDir));

            var rooms = ParseDays(casSource.OpenDays, () => File.CreateText(Path.Combine(outDir, "days.csv")));

            ParseClasses(casSource, outDir);
            ParseSchedule(casSource, outDir);
            ParseRow(casSource, outDir, rooms);
        }

        public static Tuple<string, string[]> ParseTeacherAndRooms(string input, HashSet<string> rooms)
        {
            input = input.Trim();

            if (rooms.Contains(input))
            {
                return Tuple.Create("", new[] { input });
            }

            var match = Regex.Match(input, @"^(?<teacher>.+)\s+(?<rooms>[\w]+(\,[\w]+)+)$");
            if (match.Success)
            {
                return Tuple.Create(match.Groups["teacher"].Value.Trim(), match.Groups["rooms"].Value.Trim().Split(','));
            }

            var match1 = Regex.Match(input, @"^(?<teacher>.+)\s+(?<room>\w+)$");
            if (match1.Success)
            {
                if (rooms.Contains(match1.Groups["room"].Value))
                {
                    return Tuple.Create(match1.Groups["teacher"].Value.Trim(), match1.Groups["room"].Value.Trim().Split(','));
                }
            }

            return Tuple.Create(input, new string[0]);
        }

        private static void ParseRow(ICasSource casSource, string outDir, HashSet<string> rooms)
        {
            using (var rowSource = casSource.OpenRow())
            using (var reader = new StreamReader(rowSource))
            using (var rowFile = File.CreateText(Path.Combine(outDir, "row.csv")))
            {
                var rows = reader
                    .ReadLines()
                    .Where(x => !IsNullOrWhiteSpace(x))
                    .Where(x => !x.Trim().StartsWith("Schedule Roster"))
                    .Select(sourceLine =>
                    {
                        var match = Regex.Match(sourceLine, @"\d{1,2}(,\d{1,2}){2,10}");
                        if (!match.Success)
                            throw new InvalidOperationException($"Number series has not found: {sourceLine}");

                        var parts = ParseRowStart(sourceLine.Substring(0, match.Index));
                        var start = Join(",", parts
                            .Select(p => p.Trim())
                            .Select(Utils.EscapeCsvValue));

                        var end = sourceLine.Substring(match.Index + match.Length);

                        return new
                        {
                            Start = start,
                            DayPeriods = match.Value.Split(','),
                            End = ParseTeacherAndRooms(end, rooms)
                        };
                    }).ToArray();

                var maxRoomsCount = rows.Max(x => x.End.Item2.Length);
                var roomsHeader = Join(",", Enumerable.Range(1, maxRoomsCount).Select(x => $"Room{x}"));
                var maxDayPeriods = rows.Max(x => x.DayPeriods.Length);
                var periodsHeader = Join(",", Enumerable.Range(1, maxDayPeriods).Select(x => $"Day{x}Period#"));

                rowFile.WriteLine(Join(",", new[]
                {
                    $"IDNumber,ClassCode,Subject,{periodsHeader},TeacherName",
                    roomsHeader
                }.Where(x => !IsNullOrEmpty(x))));

                rows.Iter(x =>
                {
                    var studentRooms = Join(",",
                        x.End.Item2.Extend(maxRoomsCount, "").Select(Utils.EscapeCsvValue));

                    var periods = Join(",", x.DayPeriods.Extend(maxDayPeriods, "").Select(Utils.EscapeCsvValue));

                    rowFile.WriteLine($"{x.Start},{periods},{x.End.Item1.EscapeCsvValue()},{studentRooms}");
                });
            }
        }

        private static IEnumerable<T> Extend<T>(this IEnumerable<T> seq, int count, T defaultValue = default(T))
        {
            var ind = 0;
            foreach (var item in seq)
            {
                if (ind == count) yield break;
                yield return item;
                ind += 1;
            }

            while (ind < count)
            {
                yield return defaultValue;
                ind++;
            }
        }

        public static string[] ParseRowStart(string start)
        {
            var match = Regex.Match(start, @"^\s*(?<col1>\w+)\s+(?<col2>\w+)\s+(?<col3>.*)\s*$");
            if (!match.Success) throw new InvalidOperationException($"Start hasn't been parsed {start}");
            return match.Groups.Cast<Group>().Skip(1).Select(x => x.Value).ToArray();
        }

        private static void ParseSchedule(ICasSource casSource, string outDir)
        {
            using (var scheduleInput = casSource.OpenSchedule())
            using (var parser = CreateParser(scheduleInput))
            using (var schedule = File.CreateText(Path.Combine(outDir, "schedule.csv")))
            {
                schedule.WriteLine("StudentID,Count,CourseCode,Year,SectionNumber");

                while (!parser.EndOfData)
                {
                    var fields = parser.ReadFields();
                    schedule.WriteLine(Join(",", fields.Select(Utils.EscapeCsvValue)));
                }
            }
        }

        private static void ParseClasses(ICasSource casSource, string outDir)
        {
            using (var daysStream = casSource.OpenClasses())
            using (var tfParser = CreateParser(daysStream))
            using (var classes = File.CreateText(Path.Combine(outDir, "classes.csv")))
            {
                classes.WriteLine("ClassCode,Year,NumberOfSections (Classes),Column4,Subject,Column5,MaxStudents");

                while (!tfParser.EndOfData)
                {
                    var fields = tfParser.ReadFields();

                    classes.WriteLine(Join(",", fields.Select(Utils.EscapeCsvValue)));
                }
            }
        }

        private static TextFieldParser CreateParser(Stream Stream)
        {
            var parser = new TextFieldParser(Stream);
            parser.SetDelimiters(",");
            parser.HasFieldsEnclosedInQuotes = true;
            return parser;
        }

        public static HashSet<string> ParseDays(Func<Stream> openDays, Func<StreamWriter> output)
        {
            using (var daysStream = openDays())
            using (var tfParser = CreateParser(daysStream))
            using (var days = output())
            {
                days.WriteLine("ClassCode,Year,SectionNumber,DayNumber,DayOfWeek,StartPeriod,EndPeriod,Room");

                var rooms = new HashSet<string>();

                while (!tfParser.EndOfData)
                {
                    var fields = tfParser.ReadFields();

                    fields[4] = MapWeekDay(fields[4]);

                    days.WriteLine(Join(",", fields.Select(Utils.EscapeCsvValue)));

                    rooms.Add(fields.Last());
                }

                return rooms;
            }
        }

        private static string MapWeekDay(string s)
        {
            switch (s)
            {
                case "M":
                    return "Monday";
                case "T":
                    return "Tuesday";
                case "W":
                    return "Wednesday";
                case "R":
                    return "Thursday";
                case "F":
                    return "Friday";
                default:
                    return s;
            }
        }
    }
}
