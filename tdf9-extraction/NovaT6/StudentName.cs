﻿using System.Text.RegularExpressions;

namespace Tdf9Extraction.NovaT6
{
    public sealed class StudentName
    {
        private readonly int _index;

        private bool Equals(StudentName other)
        {
            return string.Equals(Name, other.Name) && string.Equals(Surname, other.Surname) &&
                   string.Equals(TeachingGroup, other.TeachingGroup) && Year == other.Year;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((StudentName) obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = (Name != null ? Name.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (Surname != null ? Surname.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (TeachingGroup != null ? TeachingGroup.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ Year;
                return hashCode;
            }
        }

        public StudentName(string name, string surname, string group, int year, int index)
        {
            _index = index;
            Name = name.Trim().ToTitleCase();
            Surname = surname.Trim().ToUpper();
            TeachingGroup = $"{year}{group.Trim()}";
            Year = year;
        }

        public string Name { get; }
        public string Surname { get; }
        public string TeachingGroup { get; }
        public int Year { get; }
        public string StudentCode => $"E-{_index:0000}";

        public static bool TryParse(string line, int index, out StudentName name)
        {
            name = null;
            var m = Regex.Match(line, @"^(?<surname>.+),(?<name>.+)\s*\((?<year>\d+)(?<group>.+)\)$");
            if (!m.Success) return false;

            name = new StudentName(
                m.Groups["name"].Value,
                m.Groups["surname"].Value,
                m.Groups["group"].Value,
                int.Parse(m.Groups["year"].Value),
                index);

            return true;
        }
    }
}