﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reactive.Subjects;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using Serilog;
using Serilog.Core;
using Serilog.Events;
using Tdf9Extraction.Recognizer;
using Tuple = System.Tuple;

namespace Tdf9Extraction.NovaT6
{
    public sealed class NovaT6 : INovaT6
    {
        private Logger _logger;

        private readonly string _file;

        private void ConfigureLogger(string outDir)
        {
            _logger = new LoggerConfiguration()
                .WriteTo.File(
                    Path.Combine(outDir, "parsing.log"),
                    LogEventLevel.Warning)
                .WriteTo.Observers(ConfigureLogSubject)
                .WriteTo.Console(LogEventLevel.Information)
                .CreateLogger();
        }

        private readonly ReplaySubject<LogEvent> _subject = new ReplaySubject<LogEvent>();

        public IObservable<LogEvent> Events
        {
            get { return _subject; }
        }

        private IDisposable _subscription;

        private void ConfigureLogSubject(IObservable<LogEvent> events)
        {
            _subscription = events.Subscribe(_subject.OnNext);
        }

        private NovaT6(string file)
        {
            _file = file;
        }

        public static INovaT6 FromFile(string file)
        {
            return new NovaT6(file);
        }

        public string Extract(string expectedOutDir)
        {
            var outDir = GetOutDir(expectedOutDir, _file);

            ConfigureLogger(outDir);

            using (_logger)
            using (_subscription)
            {
                var students = StudentPreferences.Parse(ReadLines(), _logger).ToArray();

                WriteStudentPreferences(students, outDir);
                WriteElectiveCourses(students, outDir);

                return outDir;
            }
        }

        private static void WriteElectiveCourses(IEnumerable<StudentPreferences> students, string outDir)
        {
            var fileName = Path.Combine(outDir, "Elective courses.csv");
            File.AppendAllLines(fileName, RenderElectiveCourses(students));
        }

        public static IEnumerable<string> RenderElectiveCourses(IEnumerable<StudentPreferences> students)
        {
            yield return "Year,Code,Course name,Main requests,Reserve requests";

            var grouped = students
                .SelectMany(s => s.Preferences.Select(p => new {Student = s.StudentName, Preference = p}))
                .GroupBy(t => new
                {
                    t.Student.Year,
                    t.Preference.CourseCode,
                    t.Preference.CourseName
                })
                .OrderBy(x => x.Key.Year)
                .ThenBy(x => x.Key.CourseCode);

            foreach (var c in grouped)
            {
                var fields = RenderElectiveCourseRow(
                    c.Key.Year,
                    c.Key.CourseCode,
                    c.Key.CourseName,
                    c.Select(x => x.Preference).ToArray());

                yield return string.Join(",", fields.Select(Utils.EscapeCsvValue));
            }

        }

        private static IEnumerable<string> RenderElectiveCourseRow(
            int year,
            string courseCode,
            string courseName,
            Preference[] preferences)
        {
            yield return year.ToString();
            yield return courseCode;
            yield return courseName;
            yield return preferences.Count(x => !x.IsReserved).ToString();
            yield return preferences.Count(x => x.IsReserved).ToString();
        }

        private static void WriteStudentPreferences(IEnumerable<StudentPreferences> students, string outDir)
        {
            foreach (var sg in students.GroupBy(x => x.StudentName.Year))
            {
                var fileName = Path.Combine(outDir, $"NovaT6 Student prefs {sg.Key}.csv");

                File.AppendAllLines(fileName, RenderStudentPrefs(sg.ToArray()).ToArray());
            }
        }

        public static IEnumerable<string> RenderStudentPrefs(StudentPreferences[] students)
        {
            var maxMainPrefs = students.Max(x => x.Preferences.Count(p => !p.IsReserved));
            var maxReservePrefs = students.Max(x => x.Preferences.Count(p => p.IsReserved));

            var prefColumns = string.Join(
                ",",
                Enumerable.Range(1, maxMainPrefs + maxReservePrefs).Select(x => $"Pref{x}"));

            yield return $"Code,FullName,{prefColumns},Main count,Reserve count";

            foreach (var s in students)
            {
                yield return string.Join(
                    ",",
                    FormatStudentsRow(s, maxMainPrefs, maxReservePrefs).Select(Utils.EscapeCsvValue));
            }
        }

        private static IEnumerable<string> FormatStudentsRow(
            StudentPreferences studentPreferences,
            int maxMainPrefs,
            int maxReservePrefs)

        {
            yield return studentPreferences.StudentName.StudentCode;
            yield return $"{studentPreferences.StudentName.Surname}, {studentPreferences.StudentName.Name}";

            var mainOptions = studentPreferences.Preferences.Where(x => !x.IsReserved).ToArray();
            var reserveOptions = studentPreferences.Preferences.Where(x => x.IsReserved).ToArray();

            for (var i = 0; i < maxMainPrefs; i++)
            {
                yield return i < mainOptions.Length ? mainOptions[i].CourseCode : string.Empty;
            }

            for (var i = 0; i < maxReservePrefs; i++)
            {
                yield return i < reserveOptions.Length ? reserveOptions[i].CourseCode : string.Empty;
            }

            yield return studentPreferences.Preferences.Count(x => !x.IsReserved)
                .ToString(CultureInfo.InvariantCulture);
            yield return studentPreferences.Preferences.Count(x => x.IsReserved).ToString(CultureInfo.InvariantCulture);
        }

        private static string GetOutDir(string outDir, string fileName)
        {
            var baseName = Path.GetFileNameWithoutExtension(fileName);
            var folder = baseName
                .Yield()
                .Concat(Enumerable.Range(1, int.MaxValue).Select(x => $"{baseName}-{x}"))
                .Select(x => Path.Combine(outDir, x))
                .First(candidate => !File.Exists(candidate) && !Directory.Exists(candidate));
            return Directory.CreateDirectory(folder).FullName;
        }

        public IEnumerable<string> ReadLines()
        {
            using (var sheet = SpreadsheetDocument.Open(_file, false))
            {
                var pairs = sheet.WorkbookPart.SharedStringTablePart.SharedStringTable.Elements<SharedStringItem>()
                    .Select((x, ind) => Tuple.Create(ind, x.Text.Text))
                    .ToDictionary(x => x.Item1, x => x.Item2);

                foreach (var ws in sheet.WorkbookPart.WorksheetParts)
                {
                    foreach (var sd in ws.Worksheet.Elements<SheetData>())
                    {
                        foreach (var row in sd.Elements<Row>())
                        {
                            var cell = row.Elements<Cell>().FirstOrDefault();

                            if (cell?.DataType == null) continue;

                            if (cell.DataType.HasValue && cell.DataType.Value == CellValues.SharedString)
                            {
                                yield return pairs[int.Parse(cell.CellValue.Text)];
                            }
                            else
                            {
                                yield return cell.CellValue.Text;
                            }
                        }
                    }
                }
            }

        }
    }
}