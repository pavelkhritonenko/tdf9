﻿using System;
using System.Text.RegularExpressions;

namespace Tdf9Extraction.NovaT6
{
    public sealed class Preference : IComparable<Preference>
    {
        public Preference(int order, bool isReserved, string courseCode, string courseName)
        {
            Order = order;
            IsReserved = isReserved;
            CourseCode = courseCode.Trim();
            CourseName = courseName.Trim();
        }

        public int Order { get; }
        public bool IsReserved { get; }
        public string CourseCode { get; }
        public string CourseName { get; }

        public static bool TryParse(string line, out Preference pref)
        {
            pref = null;

            var m = Regex.Match(line, @"^(?<r>R)?(?<lvl>\d+)\s+(?<code>.+?)\s+(?<name>.+)$"); 

            if (!m.Success) return false;
            
            pref = new Preference(
                int.Parse(m.Groups["lvl"].Value),
                m.Groups["r"].Success,
                m.Groups["code"].Value,
                m.Groups["name"].Value);
            return true;
        }

        private bool Equals(Preference other)
        {
            return Order == other.Order && IsReserved == other.IsReserved &&
                   string.Equals(CourseCode, other.CourseCode) && string.Equals(CourseName, other.CourseName);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            return obj is Preference && Equals((Preference) obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = Order;
                hashCode = (hashCode * 397) ^ IsReserved.GetHashCode();
                hashCode = (hashCode * 397) ^ (CourseCode != null ? CourseCode.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (CourseName != null ? CourseName.GetHashCode() : 0);
                return hashCode;
            }
        }

        public int CompareTo(Preference other)
        {
            if (ReferenceEquals(this, other)) return 0;
            if (ReferenceEquals(null, other)) return 1;
            var isReservedComparison = IsReserved.CompareTo(other.IsReserved);
            if (isReservedComparison != 0) return isReservedComparison;
            return Order.CompareTo(other.Order);
        }
    }
}