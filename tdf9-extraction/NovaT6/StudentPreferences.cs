﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using Serilog.Core;

namespace Tdf9Extraction.NovaT6
{
    public sealed class StudentPreferences
    {
        public StudentPreferences(StudentName studentName, Preference[] preferences)
        {
            StudentName = studentName;
            Preferences = preferences ?? new Preference[0];
        }

        public StudentName StudentName { get; }
        public Preference[] Preferences { get; }

        private static IEnumerable<Tuple<StudentName, Preference>> GroupWithNames(
            IEnumerable<string> lines, 
            Logger logger)
        {
            StudentName current = null;
            var counter = 1;
            var lineCounter = 0;
            
            foreach (var line in lines)
            {
                Interlocked.Increment(ref lineCounter);
                StudentName sn;
                if (StudentName.TryParse(line, counter, out sn))
                {
                    Interlocked.Increment(ref counter);
                    current = sn;
                    yield return Tuple.Create(current, (Preference)null);
                }
                else
                {
                    Preference p;
                    if (!Preference.TryParse(line, out p))
                    {
                        logger.Warning(
                            "Error parsing line `{LineCounter}:{Line}`, expected either student name or " +
                            "preference, skipped",
                            lineCounter,
                            line);
                    }
                    else
                    {
                        yield return Tuple.Create(current, p);
                    }
                }
            }
        }

        public static IEnumerable<StudentPreferences> Parse(IEnumerable<string> lines, Logger logger)
        {
            return GroupWithNames(lines, logger).GroupBy(x => x.Item1).Select(x =>
                new StudentPreferences(x.Key, x.Select(t => t.Item2).Where(p => p != null).ToArray()));
        } 
    }
}