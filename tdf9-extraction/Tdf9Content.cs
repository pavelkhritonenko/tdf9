using System.Collections.Generic;
using System.Linq;

namespace Tdf9Extraction
{
    public sealed class Tdf9Content
    {
        public readonly Dictionary<string, Dictionary<string, string>[]> Content;
        
        public string FileName { get; private set; }

        public Tdf9Content(
            Dictionary<string, Dictionary<string, string>[]> content,
            string fileName,
            Tdf9Content[] electives)
        {
            FileName = fileName;
            Content = content;
            Electives = (electives ?? new Tdf9Content[0]).OrderBy(x => x.FileName).ToArray();
        }
        
        public Dictionary<string, string>[] Periods => GetOrCreate("Periods");

        public Dictionary<string, string>[] Classes
        {
            get { return GetOrCreate("Classes"); }
            set { Content["Classes"] = value; }
        }

        public Dictionary<string, string>[] Rooms => GetOrCreate("Rooms");
        public IEnumerable<string> Keys => Content.Keys;
        public Dictionary<string, string>[] ClassGroups => GetOrNull("ClassGroups");
        public Dictionary<string, string>[] PublishedTimetables => GetOrCreate("PublishedTimetables");
        public Dictionary<string, string>[] Days => GetOrCreate("Days");
        public Dictionary<string, string>[] Timetables => GetOrCreate("Timetables");
        public Dictionary<string, string>[] Allowances => GetOrCreate("Allowances");
        public Dictionary<string, string>[] Students => GetOrCreate("Students");
        public Dictionary<string, string>[] Teachers => GetOrCreate("Teachers");

        public Dictionary<string, string>[] GetIfExists(string name)
        {
            return Content.ContainsKey(name) ? Content[name] : null;
        }

        public Dictionary<string, string>[] GetOrNull(string name)
        {
            return !Content.ContainsKey(name) ? null : Content[name];
        }

        public Dictionary<string, string>[] GetOrCreate(string name)
        {
            // ReSharper disable once InconsistentlySynchronizedField
            if (!Content.ContainsKey(name))
            {
                lock (string.Intern(name))
                {
                    if (!Content.ContainsKey(name))
                    {
                        Content[name] = new Dictionary<string, string>[0];
                    }
                }
            }
            return Content[name];
        }

        public void RenameCollection(string src, string target)
        {
            if (!Content.ContainsKey(src)) return;
            Content[target] = Content[src];
            Content.Remove(src);
        }
        
        public Tdf9Content[] Electives { get; private set; }
    }
}