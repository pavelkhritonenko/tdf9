﻿using System;
using System.Diagnostics;
using System.IO;
using System.Linq;

namespace Tdf9Extraction
{
    public sealed class Program
    {
        private static void Main(string[] args)
        {
            var sourceFileName = args.FirstOrDefault(x => !x.StartsWith("--"));

            if (sourceFileName == null)
            {
                Console.WriteLine();
                Console.WriteLine("  Usage: tdf9-extraction.exe <input-file-name> [--wait]");
                Console.WriteLine();
                Console.WriteLine("    --wait - wait for pressing ENTER after process finished" +
                                  "(usable while debugging from VS)");
                Console.WriteLine();

                return;
            }

            var result =  Recognizer.TypeRecognizer.Extract(sourceFileName, new FileInfo(sourceFileName).DirectoryName);

            Process.Start("explorer.exe", $"\"{result}\"");

            if (args.Contains("--wait"))
            {
                Console.WriteLine("Press ENTER...");
                Console.ReadLine();
            }
        }
    }
}