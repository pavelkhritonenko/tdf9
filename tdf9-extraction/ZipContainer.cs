﻿using System;
using System.IO;
using System.IO.Compression;
using System.Linq;

namespace Tdf9Extraction
{
    public enum ScheduleVendor
    {
        Unknown = 0,
        Tdf9 = 1,
        CaS = 2
    }

    public interface IScheduleContainer
    {
        ScheduleVendor DetectFormat();
    }

    public class FolderContainer : IScheduleContainer
    {
        private readonly string _fileName;
        private readonly string _folder;

        public FolderContainer(string fileName)
        {
            if (Directory.Exists(fileName))
            {
                _folder = fileName;
            }
            else
            {
                _fileName = fileName;
                _folder = Path.GetDirectoryName(_fileName);
            }
        }

        public ScheduleVendor DetectFormat()
        {
            if ((_fileName != null) && _fileName.EndsWith(".tdf9"))
                return ScheduleVendor.Tdf9;

            if (Container.IsCasFormat(Directory.GetFiles(_folder)))
                return ScheduleVendor.CaS;

            return ScheduleVendor.Unknown;
        }


        public class ZipContainer : IScheduleContainer
        {
            private readonly string _container;

            public ZipContainer(string container)
            {
                _container = container;
            }

            public ScheduleVendor DetectFormat()
            {
                if (!File.Exists(_container))
                    throw new ArgumentException("Container doesn't exist", nameof(_container));

                try
                {
                    using (var file = File.OpenRead(_container))
                    {
                        using (var zip = new ZipArchive(file, ZipArchiveMode.Read, true))
                        {
                            var entries = zip.Entries.Select(x => x.FullName).ToArray();

                            if (Container.IsCasFormat(entries))
                                return ScheduleVendor.CaS;

                            if (entries.Any(x => x.EndsWith(".tdf9", StringComparison.OrdinalIgnoreCase)))
                                return ScheduleVendor.Tdf9;

                            return ScheduleVendor.Unknown;
                        }
                    }
                }
                catch (Exception)
                {
                    return ScheduleVendor.Unknown;
                }
            }
        }
    }
}