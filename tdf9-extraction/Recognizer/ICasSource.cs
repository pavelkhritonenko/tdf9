using System;
using System.IO;

namespace Tdf9Extraction.Recognizer
{
    public interface ICasSource : IDisposable
    {
        Stream OpenClasses();
        Stream OpenDays();
        Stream OpenRow();
        Stream OpenSchedule();
        Stream OpenScw();
        string CreateOutputDirectory(string outputFolder);
    }
}