﻿using System;
using System.Collections.Generic;
using Serilog.Events;

namespace Tdf9Extraction.Recognizer
{
    public interface INovaT6
    {
        string Extract(string outDir);
        IEnumerable<string> ReadLines();
        IObservable<LogEvent> Events { get; }
    }
}