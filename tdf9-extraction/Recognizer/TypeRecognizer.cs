﻿using System;
using System.IO;
using System.IO.Compression;
using System.Linq;
using Serilog.Events;

namespace Tdf9Extraction.Recognizer
{
    public static class TypeRecognizer
    {
        public static SourceFile Recognize(string fileOrFolder)
        {
            if (Directory.Exists(fileOrFolder))
            {
                if (Container.IsCasFormat(Directory.GetFiles(fileOrFolder)))
                {
                    return new SourceFile(new FolderCasSource(fileOrFolder));
                }
            }

            if (File.Exists(fileOrFolder))
            {
                if (fileOrFolder.EndsWith(".tdf9", StringComparison.InvariantCultureIgnoreCase))
                {
                    return new SourceFile(new DiskTdf9Source(fileOrFolder));
                }

                if (Path.GetExtension(fileOrFolder).Equals(".xlsx"))
                {
                    return new SourceFile(NovaT6.NovaT6.FromFile(fileOrFolder));
                }

                try
                {
                    using (var file = File.OpenRead(fileOrFolder))
                    using (var zip = new ZipArchive(file, ZipArchiveMode.Read))
                    {
                        if (zip.Entries.Any(x => x.FullName.EndsWith(
                            ".tdf9", StringComparison.InvariantCultureIgnoreCase)))
                        {
                            return new SourceFile(new ZippedTdf9Source(fileOrFolder));
                        }
                        if (Container.IsCasFormat(zip.Entries.Select(x => x.FullName)))
                        {
                            return new SourceFile(new ZippedCasSource(fileOrFolder));
                        }
                    }
                }
                catch (Exception)
                {
                    if (Container.IsCasFormat(Directory.GetFiles(Path.GetDirectoryName(fileOrFolder))))
                    {
                        return new SourceFile(new FolderCasSource(Path.GetDirectoryName(fileOrFolder)));
                    }
                }

                var message = new[]
                {
                    "Are you sure this file is the right one to convert?",
                    "I can only convert Timetable Solutions .tdf9 files (or zipped)",
                    "Or NovaT6 Student Options in single column format .csv or .xls.",
                    "The Source file doesn’t seem to be either of these."
                };

                return new SourceFile(new UnknownFormat(string.Join(Environment.NewLine, message)));
            }

            return new SourceFile(new UnknownFormat($"File or folder {fileOrFolder} doesn't exist"));
        }

        public static string Extract(string fileOrFolder, string outDir, Action<LogEvent> onEvent = null)
        {
            var source = Recognize(fileOrFolder);

            if (source.CasSource != null)
            {
                var cas = source.CasSource;
                var dir = cas.CreateOutputDirectory(outDir);
                CasParser.Extract(cas, dir);
                return dir;
            }

            if (source.Tdf9Source != null)
            {
                using (var streams = source.Tdf9Source.Open())
                {
                    return Tdf9Parser.ExtractTdf9FromStream(streams, fileOrFolder, outDir);
                }
            }

            if (source.NovaT6 != null)
            {
                var sub = onEvent == null ? null : source.NovaT6.Events.Subscribe(onEvent);

                using (sub)
                {
                    return source.NovaT6.Extract(outDir);
                }
            }

            if (source.UnknownFormat != null)
            {
                throw new InvalidOperationException($"Unknown format: {source.UnknownFormat.Message}");
            }

            throw new NotImplementedException();
        }
    }
}