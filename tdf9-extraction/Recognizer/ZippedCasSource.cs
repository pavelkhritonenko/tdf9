using System;
using System.IO;
using System.IO.Compression;
using System.Linq;

namespace Tdf9Extraction.Recognizer
{
    public class ZippedCasSource : ICasSource
    {
        private readonly ZipArchive _zip;
        private readonly FileStream _stream;
        private readonly string _file;

        public ZippedCasSource(string file)
        {
            _file = file;
            _stream = File.OpenRead(file);
            _zip = new ZipArchive(_stream);
        }

        public void Dispose()
        {
            _zip.Dispose();
            _stream.Dispose();
        }

        private Stream OpenFile(string fileName)
        {
            return _zip.Entries
                .SingleOrDefault(x => x.FullName.EndsWith(fileName, StringComparison.InvariantCultureIgnoreCase))
                ?.Open();
        }

        public Stream OpenClasses()
        {
            return OpenFile("classes.txt");
        }

        public Stream OpenDays()
        {
            return OpenFile("days.txt");
        }

        public Stream OpenRow()
        {
            return OpenFile("row.txt");
        }

        public Stream OpenSchedule()
        {
            return OpenFile("schedule.txt");
        }

        public Stream OpenScw()
        {
            return OpenFile("scw.txt");
        }

        public string CreateOutputDirectory(string outputFolder)
        {
            var folder = "out".Yield()
                .Concat(Enumerable.Range(1, int.MaxValue).Select(x => $"out-{x}"))
                .Select(x => Path.Combine(outputFolder ?? Path.GetDirectoryName(_file), x))
                .First(candidate => !File.Exists(candidate) && !Directory.Exists(candidate));
            return Directory.CreateDirectory(folder).FullName;
        }
    }
}
