using System;
using System.IO;

namespace Tdf9Extraction.Recognizer
{
    public sealed class Tdf9Container : IDisposable
    {
        public Tdf9Container(Stream mainSchedule, Tuple<Stream, string>[] preferencesStreams)
        {
            MainSchedule = mainSchedule;
            PreferencesStreams = preferencesStreams;
        }

        public Stream MainSchedule { get; }
        public Tuple<Stream, string>[] PreferencesStreams { get; }

        public void Dispose()
        {
            MainSchedule?.Dispose();
            foreach (var s in PreferencesStreams)
                s.Item1.Dispose();
        }
    }

    public interface ITdf9Source : IDisposable
    {
        Tdf9Container Open();
    }
}